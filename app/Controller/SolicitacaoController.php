<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class SolicitacaoController extends AppController
{

    public $components = array('Paginator');
    public $uses = array('TipoMaterial', 'UnidadeMedida', 'Solicitacao', 'AgendaDisponivel', 'SolicitacaoUsuario', 'TipoMaterialValor', 'SolicitacaoPonto');

    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->Auth->allow('*');
        $this->Auth->allow();

    }

    public function index()
    {
        $tipoMaterial = $this->TipoMaterial->find('all');
        $this->set(compact('tipoMaterial'));
    }

    public function quantidade()
    {

        if ($this->request->is('post')) {

            foreach ($this->request->data['tipoMaterial']['id'] as $id) {
                $arrMaterial = $this->TipoMaterial->findByid($id);
                $arrSolicitacao['Solicitacao'][$id] = array(
                    'tipo_material_id' => $id,
                    'quantidade' => '',
                    'unidade_medida_id' => '',
                    'descricao' => $arrMaterial['TipoMaterial']['descricao'],
                );
            }

            $unidadeMedida = $this->UnidadeMedida->find('list', array('fields' => array('id', 'descricao')));
            $this->set(compact('arrSolicitacao', 'unidadeMedida'));

        } else {

            $this->redirect(array('action' => 'index'));

        }
    }

    public function agenda()
    {
        if ($this->request->is('post')) {

            $solicita = $this->request->data;
            $this->Session->write('solicita', $solicita);

        } else {
            $this->redirect(array('action' => 'index'));
        }
    }

    public function confirmacao()
    {
        if ($this->request->is('post')) {
            $agenda = $this->request->data;
            $solicitacoes = $this->Session->read('solicita');

            $db = $this->AgendaDisponivel->getDataSource();
            $db->begin();

            try {

                $formSolicitacao = array(
                    'SolicitacaoUsuario' => array(
                        'usuario_id' => $this->Auth->user('Usuario.id'),
                        'ip' => $this->request->clientIp()),
                );
                $this->SolicitacaoUsuario->create();
                $this->SolicitacaoUsuario->save($formSolicitacao);
                $solicitacao_do_usuario_id = $this->SolicitacaoUsuario->id;

                foreach ($solicitacoes['Solicitacao'] as $arrSolicitacao) {

                    $arrSolicitacao['usuario_id'] = $this->Auth->user('Usuario.id');
                    $arrSolicitacao['status_id'] = 1;
                    $arrSolicitacao['solicitacao_usuario_id'] = $solicitacao_do_usuario_id;
                    $arrForm['Solicitacao'] = $arrSolicitacao;

                    $this->Solicitacao->create();
                    $this->Solicitacao->save($arrForm);
                    $solicitacao_id = $this->Solicitacao->id;
                    $arrForm['Solicitacao']['solicitacao_id'] = $solicitacao_id;

                    $this->salvarPontuacao($arrForm);

                    //Se der qualquer problema invalida toda a operação
                    if (!$this->salvarDiaDeAgendamento($agenda, $solicitacao_id)) {
                        $db->rollback();
                        throw new Exception("Invalidou a operação");

                        exit;
                    };

                };

                $db->commit();

                $this->Session->setFlash('Ok, solicitação de coleta realizada com sucesso, agora essa solicitação será enviada a cooperativa de coletores mais próxima sua residência.', 'success');
                $this->redirect(array('controller' => 'Painel', 'action' => 'index'));

            } catch (Exception $e) {
                $db->rollback();
            }

        } else {

            $this->redirect(array('action' => 'index'));

        }
    }

    private function salvarPontuacao($arrPontuacao)
    {
        try {

            $arrValorKilo = $this->TipoMaterialValor->findBytipo_material_id($arrPontuacao['Solicitacao']['tipo_material_id']);
            $cd_valor_kilo = $arrValorKilo['TipoMaterialValor']['valor_kilo_litro'];

            $arrMultiplicador = $this->UnidadeMedida->findByid($arrPontuacao['Solicitacao']['unidade_medida_id']);
            $cd_multiplicador = $arrMultiplicador['UnidadeMedida']['multiplicador'];

            $quantidade = $arrPontuacao['Solicitacao']['quantidade'];

            $pontos = ($cd_multiplicador / $cd_valor_kilo) * $quantidade;
            $arrFormSave['SolicitacaoPonto'] = $arrPontuacao['Solicitacao'];
            $arrFormSave['SolicitacaoPonto']['ponto'] = $pontos;

            $this->SolicitacaoPonto->create();
            $this->SolicitacaoPonto->save($arrFormSave);

            return true;
        } catch (Exception $e) {
            return false;
        }

    }

    /**
     * Cada dia possui um horário especifico, então devo salvar esse horário especifico
     */
    private function salvarDiaDeAgendamento($arrAgenda, $solicitacao_id)
    {
        foreach ($arrAgenda['Agendamento']['dia_semana'] as $dia_da_semana) {

            //se der qualquer problema invalida toda a operação
            if (!$this->salvarAgenda($dia_da_semana, $arrAgenda, $solicitacao_id)) {
                return false;
                exit;
            };
        }
        return true;
    }

    private function salvarAgenda($dia_da_semana, $arrAgenda, $solicitacao_id)
    {

        foreach ($arrAgenda['Agendamento']['horario'] as $horario) {

            $arrForm['AgendaDisponivel']['dia_semana'] = $dia_da_semana;
            $arrForm['AgendaDisponivel']['horario'] = $horario;
            $arrForm['AgendaDisponivel']['solicitacao_id'] = $solicitacao_id;

            $this->AgendaDisponivel->create();

            //Se ocorrer qualquer erro então deve retornar imediatamente com false
            if (!$this->AgendaDisponivel->save($arrForm)) {
                return false;
                exit;
            };

        }

        return true;
    }

}
