<?php
App::uses('AppController', 'Controller');
/**
 * Enderecos Controller
 *
 * @property Endereco $Endereco
 * @property PaginatorComponent $Paginator
 */
class EnderecosController extends AppController
{

    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->Auth->allow('*');
        $this->Auth->allow();
    }
/**
 * Components
 *
 * @var array
 */
    public $components = array('Paginator');

    public function buscar($cep)
    {
        $this->Endereco->recursive = 2;
        $endereco                  = $this->Endereco->findBycep($cep);
        if ($endereco) {

            echo json_encode($endereco);
            die;
        }
        echo null;
        die;
    }
}
