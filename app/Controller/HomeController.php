<?php
App::uses('AppController', 'Controller');
/**
 * Homes Controller
 *
 * @property Home $Home
 * @property PaginatorComponent $Paginator
 * @property nComponent $n
 */
class HomeController extends AppController
{

/**
 * Components
 *
 * @var array
 */
    public $components = array('Paginator');

    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->Auth->allow('*');
        $this->Auth->allow();
    }

    public function index()
    {

    }
    public function comofunciona()
    {

    }
}
