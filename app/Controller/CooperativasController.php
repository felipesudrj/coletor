<?php
App::uses('AppController', 'Controller');
/**
 * Cooperativas Controller
 *
 * @property Cooperativa $Cooperativa
 * @property PaginatorComponent $Paginator
 */
class CooperativasController extends AppController {

/**
 * Components
 *
 * @var array
 */
	public $components = array('Paginator');

public function index() {
		$this->Cooperativa->recursive = 0;
		$this->set('cooperativas', $this->Paginator->paginate());
	}

	public function view($id = null) {
		if (!$this->Cooperativa->exists($id)) {
			throw new NotFoundException(__('Invalid cooperativa'));
		}
		$options = array('conditions' => array('Cooperativa.' . $this->Cooperativa->primaryKey => $id));
		$this->set('cooperativa', $this->Cooperativa->find('first', $options));
	}


	public function add() {
		if ($this->request->is('post')) {
			$this->Cooperativa->create();
			if ($this->Cooperativa->save($this->request->data)) {
				$this->Session->setFlash(__('Registro salvo com sucesso.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Desculpe, ocorreu um erro e o registro não pode ser salvo. por favor, tente novamente.'), 'default', array('class' => 'alert alert-danger'));
			}
		}
		$enderecos = $this->Cooperativa->Endereco->find('list');
		$this->set(compact('enderecos'));
	}


	public function edit($id = null) {
		if (!$this->Cooperativa->exists($id)) {
			throw new NotFoundException(__('Invalid cooperativa'));
		}
		if ($this->request->is(array('post', 'put'))) {
			if ($this->Cooperativa->save($this->request->data)) {
				$this->Session->setFlash(__('Alterado com sucesso.'), 'default', array('class' => 'alert alert-success'));
				return $this->redirect(array('action' => 'index'));
			} else {
				$this->Session->setFlash(__('Não foi possivel realizar a alteração. por favor, tente novamente.'), 'default', array('class' => 'alert alert-danger'));
			}
		} else {
			$options = array('conditions' => array('Cooperativa.' . $this->Cooperativa->primaryKey => $id));
			$this->request->data = $this->Cooperativa->find('first', $options);
		}
		$enderecos = $this->Cooperativa->Endereco->find('list');
		$this->set(compact('enderecos'));
	}

	public function delete($id = null) {
		$this->Cooperativa->id = $id;
		if (!$this->Cooperativa->exists()) {
			throw new NotFoundException(__('Invalid cooperativa'));
		}
		$this->request->onlyAllow('post', 'delete');
		if ($this->Cooperativa->delete()) {
			$this->Session->setFlash(__('Excluido com sucesso.'), 'default', array('class' => 'alert alert-success'));
		} else {
			$this->Session->setFlash(__('Não foi possível realizar a exclusão. por favor, tente novamente.'), 'default', array('class' => 'alert alert-danger'));
		}
		return $this->redirect(array('action' => 'index'));
	}}
