<?php
App::uses('AppController', 'Controller');
/**
 * Enderecos Controller
 *
 * @property Endereco $Endereco
 * @property PaginatorComponent $Paginator
 */
class PainelController extends AppController
{
    public $uses = array('Solicitacao', 'SolicitacaoPonto');
    public function index()
    {
        $breadcrub = array(
            'titulo' => 'Painel',
            'links' => array(
                '/' => 'Inicio',
            ),
        );
        $conditions = array(
            'conditions' => array(
                'Solicitacao.usuario_id' => $this->Auth->user('Usuario.id'),
            ),

        );
        $this->paginate = $conditions;
        $solicitacoes = $this->Paginate('Solicitacao');

        $totalPontosPendentes = $this->SolicitacaoPonto->totalPontosPendentes($this->Auth->user('Usuario.id'));

        $this->set(compact('breadcrumb', 'solicitacoes', 'totalPontosPendentes'));

    }

    public function solicitacoesUsuario()
    {

        $breadcrub = array(
            'titulo' => 'Painel',
            'links' => array(
                '/' => 'Inicio',
                '/painel/solicitacoesUsuario' => 'Solicitações',
            ),
        );

        $conditions = array(
            'conditions' => array(
                'Solicitacao.usuario_id' => $this->Auth->user('Usuario.id'),
            ),
        );
        $this->paginate = $conditions;
        $solicitacoes = $this->Paginate('Solicitacao');

        $this->set(compact('breadcrumb', 'solicitacoes'));

    }
}
