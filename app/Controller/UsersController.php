<?php
App::uses('AppController', 'Controller');
/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class UsersController extends AppController
{

    public $components = array('Paginator');
    public $uses       = array('User', 'Estado', 'Bairro', 'Estado', 'Cidade', 'Endereco', 'Usuario');

    public function beforeFilter()
    {
        parent::beforeFilter();
        $this->Auth->allow('*');
        $this->Auth->allow();
    }

    public function index()
    {

        $this->User->recursive = 0;
        $this->set('users', $this->Paginator->paginate());
    }

    public function logout()
    {
        $this->redirect($this->Auth->logout());
    }

    public function login()
    {

        if ($this->request->is('post')) {

            $form       = $this->request->data;
            $conditions = array('conditions' => array('User.password' => $form['User']['password']));
            $retorno    = $this->User->find('all', $conditions);

            if ($this->Auth->login()) {

                return $this->redirect($this->Auth->redirect());
            }

            $this->Session->setFlash('Desculpe, mas você digitou seu email ou senha de acesso errados.', 'error');

        }

    }

    public function cadastro()
    {

        if ($this->request->is('post')) {

            $form = $this->request->data;

            //Se o endereco_id for null então cadastrar o novo endereço
            if (empty($form['Usuario']['endereco_id'])) {

                $form['Usuario']['endereco_id'] = $this->salvarEndereco($form);
            }

            try {

                if ($this->User->SaveAll($form)) {

                    $this->Session->setFlash('Ok, seu interesse em participar da nossa rede foi realizado, agora acesse seu email de confirme seu cadastro. :)', 'success');

                    $this->redirect(array('action' => 'cadastro'));

                } else {

                    $this->Session->setFlash(':( Ocorreu um problema ao realizar seu cadastro, tente novamente mais tarde ou reporte esse erro no email contato@coletor.com.br Código: 55-US', 'error');
                };
            } catch (Exception $e) {

                $codigo = $e->getcode();

                $this->Session->setFlash('Ops!!!! Desculpe, Ocorreu um problema ao realizar seu cadastro, tente novamente mais tarde ou reporte esse erro no email contato@coletor.com.br. Código ' . $codigo, 'error');

            }

            $erros = $this->Usuario->User->invalidFields();
            $this->set('validador', $erros);

        }
        $estados = $this->Estado->find('list', array('fields' => array('estado_id', 'descricao')));
        $this->set(compact('estados'));

    }

    public function salvarEndereco($form)
    {
        //extratir elemento para variavéis
        extract($form['Usuario']);

        $db = $this->Endereco->getDataSource();

        //verificar se o cep existe na database
        $busca = $this->Endereco->findBycep($cep);
        if ($busca) {
            return $busca['Endereco']['endereco_id'];
        };

        try {

            //pegar ID do estado e gravar a cidade
            $save['Cidade']['estado_id'] = $estado;
            $save['Cidade']['descricao'] = $cidade;
            $this->Cidade->save($save);

            //Pegar o ID da cidade e gravar o bairro
            $save['Bairro']['cidade_id'] = $this->Cidade->id;
            $save['Bairro']['descricao'] = $bairro;

            //Pegar o ID da bairro e gravar o endereco
            $this->Bairro->save($save);

            $save['Endereco']['bairro_id']  = $this->Bairro->id;
            $save['Endereco']['cep']        = $cep;
            $save['Endereco']['logradouro'] = $endereco;

            //gravar o endereco
            $this->Endereco->save($save);

            //retornar o id do endereco

            $endereco_id = $this->Endereco->id;

            $db->commit();
            return $endereco_id;

        } catch (Exception $e) {

            $db->rollback();
            echo $msg = h('Erro:5533841 - Ocorreu um erro ao realizar o cadastro, tente novamente mais tarde ou reporte esse erro no email contato@coletor.com.br');
            die();
            return $msg;
        }

    }

}
