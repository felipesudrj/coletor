 <?php $this->start('css');?>
   <!-- CSS Implementing Plugins -->
    <link rel="stylesheet" href="/plugins/font-awesome-4/css/font-awesome.css">
    <link rel="stylesheet" href="/plugins/flexslider/flexslider.css">
    <link rel="stylesheet" href="/plugins/bxslider/jquery.bxslider.css">
    <link rel="stylesheet" href="/plugins/layer_slider/css/layerslider.css" type="text/css">
    <!-- CSS Theme -->
    <link rel="stylesheet" href="/css/themes/default.css" id="style_color">
    <link rel="stylesheet" href="/css/themes/headers/default.css" id="style_color-header-1">
<?php $this->end();?>

<?php $this->start('script');?>

    <!-- JS Implementing Plugins -->
    <script type="text/javascript" src="/plugins/flexslider/jquery.flexslider-min.js"></script>
    <script type="text/javascript" src="/plugins/bxslider/jquery.bxslider.js"></script>
    <!-- Layer Slider -->
    <script src="/plugins/layer_slider/jQuery/jquery-easing-1.3.js" type="text/javascript"></script>
    <script src="/plugins/layer_slider/jQuery/jquery-transit-modified.js" type="text/javascript"></script>
    <script src="/plugins/layer_slider/js/layerslider.transitions.js" type="text/javascript"></script>
    <script src="/plugins/layer_slider/js/layerslider.kreaturamedia.jquery.js" type="text/javascript"></script>
    <!-- End Layer Slider -->
    <!-- JS Page Level -->
    <script type="text/javascript" src="/js/app.js"></script>
    <script type="text/javascript" src="/js/pages/index.js"></script>




    <script type="text/javascript">
        jQuery(document).ready(function() {
            App.init();
            App.initSliders();
            App.initBxSlider();
            Index.initLayerSlider();
        });
    </script>

<?php $this->end();?>


<?php
$this->start('slider');
     echo $this->element('slider');
$this->end();
?>


<!-- Service Blocks -->
	<div class="row margin-bottom-10">
    	<div class="col-md-4 service-alternative">
    		<div class="service">
                <i class="fa fa-hand-pointer-o service-icon"></i>
    			<div class="desc">
    				<h4>Solicitação de coleta</h4>
                    <p>Você se cadastra no <strong>Coletor Online</strong>, informa o tipo de material disponível para reciclagem que possui, informa a quantidade, agenda o melhor dia ou horário para que um funcionário realize a coleta.</p>
    			</div>
    		</div>
    	</div>
    	<div class="col-md-4 service-alternative">
    		<div class="service">
                <i class="fa fa-truck service-icon"></i>
    			<div class="desc">
    				<h4>Coleta residêncial</h4>
                    <p>No dia e horário agendado*, um funcionário ou o veiculo de coleta seletiva da sua cidade vai até sua residencia e realiza a remoção do material. Você também pode ir diretamente tambem a um ponto de triagem.</p>
    			</div>
    		</div>
    	</div>
    	<div class="col-md-4 service-alternative">
    		<div class="service">
                <i class="fa fa-smile-o service-icon"></i>
    			<div class="desc">
    				<h4>Pontuação e prêmios</h4>
                    <p>Para cada doação realizada você ganha uma pontuação, quanto mais pontos você acumula mas chances tem de receber prêmios de incentivo por contribuir para o bem do meio ambiente.</p>
    			</div>
    		</div>
    	</div>
	</div><!--/row-->
	<!-- End Service Blokcs -->

    <!-- Recent Works -->
    <div class="row margin-bottom-40">
        <div class="col-lg-12">
            <div class="headline"><h2>Últimas noticias</h2></div>
            <ul class="bxslider recent-work">
                <li>
                    <a href="#">
                        <em class="overflow-hidden"><img src="/img/main/1.jpg" alt="" class="col-md-6 img-responsive" /></em>
                        <span>
                            <strong>Coleta seletiva em seu bairro</strong>
                            <i>Artigos sobre o assunto</i>
                        </span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <em class="overflow-hidden"><img src="/img/main/2.jpg" alt=""  class="col-md-6 img-responsive" /></em>
                        <span>
                            <strong>Lixo o grande vilão do planeta</strong>
                            <i>Responsive Bootstrap Template</i>
                        </span>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <em class="overflow-hidden"><img src="/img/main/3.jpg" alt="" class="col-md-6 img-responsive" /></em>
                        <span>
                            <strong>Sucesso mundial, coletor online</strong>
                            <i>Presente em mais de 80 países</i>
                        </span>
                    </a>
                </li>

            </ul>
        </div>
    </div><!--/row-->
    <!-- End Recent Works -->

	<!-- Info Blokcs -->
	<div class="row margin-bottom-10">
    	<!-- Welcome Block -->
		<div class="col-md-8">
			<div class="headline"><h2>Faça sua parte</h2></div>
            <div class="row">
                <div class="col-sm-4">
                    <img class="img-responsive margin-bottom-20" src="/img/new/img15.jpg" alt="" />
                </div>
                <div class="col-sm-8">
                    <p>Achei incrivél a idéia, ajuda muita gente esse sistema tanto quem consome e produz o lixo, como aqueles que sobrevivem do trabalho de reciclagem. Me cadastrastrei e uso semanalmente.</p>

                </div>
            </div>

            <blockquote class="hero-unify">
                <p>Bruna Almeida Sales</p>
                <small>São Paulo - SP</small>
            </blockquote>
        </div><!--/col-md-8-->


	</div><!--/row-->
	<!-- End Info Blokcs -->

	<!-- Our Clients -->
	<div id="clients-flexslider" class="flexslider home clients">
        <div class="headline"><h2>Eco-Parceiros</h2></div>
		<ul class="slides">
			<li>
                <a href="#">
                    <img src="/img/clients/hp_grey.png" alt="" />
                    <img src="/img/clients/hp.png" class="color-img" alt="" />
                </a>
            </li>
			<li>
                <a href="#">
                    <img src="/img/clients/igneus_grey.png" alt="" />
                    <img src="/img/clients/igneus.png" class="color-img" alt="" />
                </a>
            </li>
			<li>
                <a href="#">
                    <img src="/img/clients/vadafone_grey.png" alt="" />
                    <img src="/img/clients/vadafone.png" class="color-img" alt="" />
                </a>
            </li>
			<li>
                <a href="#">
                    <img src="/img/clients/walmart_grey.png" alt="" />
                    <img src="/img/clients/walmart.png" class="color-img" alt="" />
                </a>
            </li>
			<li>
                <a href="#">
                    <img src="/img/clients/shell_grey.png" alt="" />
                    <img src="/img/clients/shell.png" class="color-img" alt="" />
                </a>
            </li>
			<li>
                <a href="#">
                    <img src="/img/clients/natural_grey.png" alt="" />
                    <img src="/img/clients/natural.png" class="color-img" alt="" />
                </a>
            </li>
			<li>
                <a href="#">
                    <img src="/img/clients/aztec_grey.png" alt="" />
                    <img src="/img/clients/aztec.png" class="color-img" alt="" />
                </a>
            </li>
			<li>
                <a href="#">
                    <img src="/img/clients/gamescast_grey.png" alt="" />
                    <img src="/img/clients/gamescast.png" class="color-img" alt="" />
                </a>
            </li>
			<li>
                <a href="#">
                    <img src="/img/clients/cisco_grey.png" alt="" />
                    <img src="/img/clients/cisco.png" class="color-img" alt="" />
                </a>
            </li>
			<li>
                <a href="#">
                    <img src="/img/clients/everyday_grey.png" alt="" />
                    <img src="/img/clients/everyday.png" class="color-img" alt="" />
                </a>
            </li>
			<li>
                <a href="#">
                    <img src="/img/clients/cocacola_grey.png" alt="" />
                    <img src="/img/clients/cocacola.png" class="color-img" alt="" />
                </a>
            </li>
			<li>
                <a href="#">
                    <img src="/img/clients/spinworkx_grey.png" alt="" />
                    <img src="/img/clients/spinworkx.png" class="color-img" alt="" />
                </a>
            </li>
			<li>
                <a href="#">
                    <img src="/img/clients/shell_grey.png" alt="" />
                    <img src="/img/clients/shell.png" class="color-img" alt="" />
                </a>
            </li>
			<li>
                <a href="#">
                    <img src="/img/clients/natural_grey.png" alt="" />
                    <img src="/img/clients/natural.png" class="color-img" alt="" />
                </a>
            </li>
			<li>
                <a href="#">
                    <img src="/img/clients/gamescast_grey.png" alt="" />
                    <img src="/img/clients/gamescast.png" class="color-img" alt="" />
                </a>
            </li>
			<li>
                <a href="#">
                    <img src="/img/clients/everyday_grey.png" alt="" />
                    <img src="/img/clients/everyday.png" class="color-img" alt="" />
                </a>
            </li>
			<li>
                <a href="#">
                    <img src="/img/clients/spinworkx_grey.png" alt="" />
                    <img src="/img/clients/spinworkx.png" class="color-img" alt="" />
                </a>
            </li>
		</ul>
	</div><!--/flexslider-->
	<!-- End Our Clients -->