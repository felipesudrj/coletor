<?php echo $this->element('sidebar-painel');?>

<div class="col-md-9">

    <div class="margin-bottom-10"></div>

    <div class="headline"><h2>Agendamento</h2></div>
    <div class="alert alert-block alert-danger fade in">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4>Leia com atenção!</h4>
        <p>
            O agendamento de coleta funciona da seguinte forma: Você acabou de informar o material que tem disponível para coleta, agora você irá informar qual é o melhor dia da semana e horário para que esse material seja coletado em sua residencia.
        </p>
        <p>
            A partir desse momento sua solicitação será enviada a cooperativa de coleta seletiva mais próxima da sua residência, ela será notificada e irá realizar o processo de coleta.
        </p>
        <p>
            Para cada solicitação, você receberá uma quantidade X de pontos, porém esses pontos só serão computados e confirmados em seus saldos de pontos quando a cooperativa ou o responsável pela coleta confirmar que realmente você entregou o material na quantidade informada ou pelo menos bem próxima a quantidade informada.
        </p>

    </div>

    <p>
        <table>
            <thead>
                <th>Material</th>
                <th>Quantidade</th>

            </thead>
        </table>
    </p>

    <form action="/solicitacao/confirmacao" method="post">
      <div class="row">
        <div class="form-group">
            <label for="inputEmail1" class="col-lg-2 control-label"><?php echo ucfirst(h('Dia da semana'));?></label>
            <div class="col-lg-10">
                <?php
                $semana = array('Segunda','Terça','Quarta','Quinta','Sexta');
                echo $this->Form->input('Agendamento.dia_semana',
                array(
                'options'=>$semana,
                'multiple'=>true,
                'label'=>false,
                'div'=>false,
                'class'=>'data form-control chosen'));
                ?>
            </div>
        </div>
    </div>
    <hr/>
    <div class="row">

        <div class="form-group">
            <label for="inputEmail1" class="col-lg-2 control-label"><?php echo ucfirst(h('Melhor horário'));?></label>
            <div class="col-lg-10">
                <?php
                $semana = array('Manhã','Tarde','Noite');
                echo $this->Form->input('Agendamento.horario',
                array(
                'options'=>$semana,
                'multiple'=>true,
                'label'=>false,
                'div'=>false,
                'class'=>'data form-control chosen'));
                ?>
            </div>
        </div>

    </div>
<br/><br/>
            <div class="row text-right">
        <button type="submit" class="btn btn-u btn-lg">Confirmar</button>
        </div>

</form>
</div>


<?php $this->start('script');?>
<script type="text/javascript" src="/plugins/chosen/chosen.jquery.min.js"></script>
<script type="text/javascript">
    $('.chosen').chosen({placeholder_text_multiple:'Selecione os dias da semana'});
</script>
<?php $this->end();?>

<?php $this->start('css');?>
<link rel="stylesheet" href="/plugins/chosen/chosen.min.css">
<link rel="stylesheet" href="/plugins/chosen/chosen-bootstrap.css">
<style type="text/css">
    label{
        font-size: 16px;
    }
</style>
<?php $this->end();?>
