<!--=== Footer ===-->
<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-4 md-margin-bottom-40">
                <!-- About -->
                <div class="headline"><h2>Sobre nós</h2></div>
                <p class="margin-bottom-25 md-margin-bottom-40">Somos um grupo de amigos, programadores envolvidos com causas ambientes e extremamente procupado em melhorar a qualidade de vida das pessoas e o nosso meio ambiente.</p>


            </div><!--/col-md-4-->

            <div class="col-md-4 md-margin-bottom-40">
                <div class="posts">
                    <div class="headline"><h2>Postagens recentes</h2></div>
                    <dl class="dl-horizontal">
                        <dt><a href="#"><img src="/img/sliders/elastislide/6.jpg" alt="" /></a></dt>
                        <dd>
                            <p><a href="#">Anim moon officia Unify is an incredibly beautiful responsive Bootstrap Template</a></p>
                        </dd>
                    </dl>
                    <dl class="dl-horizontal">
                    <dt><a href="#"><img src="/img/sliders/elastislide/10.jpg" alt="" /></a></dt>
                        <dd>
                            <p><a href="#">Anim moon officia Unify is an incredibly beautiful responsive Bootstrap Template</a></p>
                        </dd>
                    </dl>
                    <dl class="dl-horizontal">
                    <dt><a href="#"><img src="/img/sliders/elastislide/11.jpg" alt="" /></a></dt>
                        <dd>
                            <p><a href="#">Anim moon officia Unify is an incredibly beautiful responsive Bootstrap Template</a></p>
                        </dd>
                    </dl>
                </div>
            </div><!--/col-md-4-->

            <div class="col-md-4">
                <!-- Monthly Newsletter -->
                <div class="headline"><h2>Ajude nosso projeto</h2></div>
                <address class="md-margin-bottom-40">
                    Banco HSBC <br />
                    Agencia: 0126<br/>
                    Conta:088855-6<br />
                    Felipe de Oliveira da Silva<br/>
                    email: felipe.devel@gmail.com
                </address>

                <!-- Stay Connected -->
                <div class="headline"><h2>Contribua</h2></div>
                <input type="button" class="btn btn-xs btn-success" value="Colabore com nosso projeto - Faça sua doação"/>
            </div><!--/col-md-4-->
        </div><!--/row-->
    </div><!--/container-->
</div><!--/footer-->
<!--=== End Footer ===-->

<!--=== Copyright ===-->
<div class="copyright">
	<div class="container">
		<div class="row">
			<div class="col-md-6">
	            <p class="copyright-space">
                    2013 &copy; Unify. ALL Rights Reserved. <br/>
                    * O cumprimento do horário de coleta depende da disponibilidade dos coletores de sua cidade ou região.
                </p>
			</div>
			<div class="col-md-6">
				<a href="index.html">
                    <img id="logo-footer" src="/img/logo2-default.png" class="pull-right" alt="" />
                </a>
			</div>
		</div><!--/row-->
	</div><!--/container-->
</div><!--/copyright-->
<!--=== End Copyright ===-->
