<!--=== Slider ===-->
<div class="layer_slider">
    <div id="layerslider-container-fw">
        <div id="layerslider" style="width: 100%; height: 500px; margin: 0px auto; ">
            <!--First Slide-->
            <div class="ls-layer" style="slidedirection: right; transition2d: 24,25,27,28; ">

                <img src="/img/sliders/layer/bg1.jpg" class="ls-bg" alt="Slide background">

                <img src="/img/mockup/lixeiras.png" alt="Slider Image" class="ls-s-1" style=" top:110px; left: 240px; slidedirection : left; slideoutdirection : bottom; durationin : 1500; durationout : 1500; ">

                <img src="/img/mockup/arvore.png" alt="Slider image" class="ls-s-1" style=" top:60px; left: 40px; slidedirection : left; slideoutdirection : bottom; durationin : 2500; durationout : 2500; ">

                <span class="ls-s-1" style=" text-transform: uppercase; line-height: 45px; font-size:35px; color:#fff; top:200px; left: 590px; slidedirection : top; slideoutdirection : bototm; durationin : 3500; durationout : 3500; ">
                    Coletor Online <br> faça sua parte para melhorar o meio ambiente
                </span>

                <a class="btn-u btn-u-orange ls-s1" href="/cadastro" style=" padding: 10px 20px; font-size:25px; top:380px; left: 590px; slidedirection : bottom; slideoutdirection : top; durationin : 3500; durationout : 3500; ">
                    Cadastre-se agora
                </a>
            </div>
            <!--End First Slide-->



            <!--Third Slide-->
            <div class="ls-layer" style="slidedirection: right; transition2d: 92,93,105; ">
                <img src="/img/sliders/layer/bg2.jpg" class="ls-bg" alt="Slide background">

                <span class="ls-s-1" style=" color: #777; line-height:45px; font-weight: 200; font-size: 35px; top:100px; left: 50px; slidedirection : top; slideoutdirection : bottom; durationin : 1000; durationout : 1000; ">
                    Colabora com o processo de coleta seletiva <br> da sua cidade
                </span>

                <a class="btn-u btn-u-green ls-s-1" href="/como-funciona#projeto" style=" padding: 9px 20px; font-size:25px; top:220px; left: 50px; slidedirection : bottom; slideoutdirection : bottom; durationin : 2000; durationout : 2000; ">
                    Conheça nosso projeto
                </a>

                <img src="/img/mockup/arvore.png" alt="Slider Image" class="ls-s-1" style=" top:30px; left: 670px; slidedirection : right; slideoutdirection : bottom; durationin : 3000; durationout : 3000; ">
            </div>
            <!--End Third Slide-->
        </div>
    </div>
</div><!--/layer_slider-->
<!--=== End Slider ===-->


<!--=== Purchase Block ===-->
    <div class="purchase">
        <div class="container">
            <div class="row">
                <div class="col-md-9">
                    <span>Coletor online.</span>
                    <p>Junte-se a outras milhares de pessoas que com pequenos gestos ajudam a salvar nosso planeta. O site coletor online foi desenvolvido para auxilia-lo e incentiva-lo a auxiliar na reciclagem de material sólido.</p>
                </div>
                <div class="col-md-3">
                    <a href="#" class="btn-buy hover-effect">Cadastre-se agora</a>
                </div>
            </div>
        </div>
    </div><!--/row-->
    <!-- End Purchase Block -->
