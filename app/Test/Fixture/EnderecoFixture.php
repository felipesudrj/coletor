<?php
/**
 * EnderecoFixture
 *
 */
class EnderecoFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'endereco';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'endereco_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10, 'key' => 'primary'),
		'bairro_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10, 'key' => 'index'),
		'cep' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 8, 'key' => 'index', 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'logradouro' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 200, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'endereco_id', 'unique' => 1),
			'fk_bairro_endereco' => array('column' => 'bairro_id', 'unique' => 0),
			'fk_cep' => array('column' => 'cep', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'endereco_id' => 1,
			'bairro_id' => 1,
			'cep' => 'Lorem ',
			'logradouro' => 'Lorem ipsum dolor sit amet'
		),
	);

}
