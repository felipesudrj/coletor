<?php
App::uses('AppModel', 'Model');
/**
 * Solicitacao Model
 *
 * @property TipoMaterial $TipoMaterial
 * @property UnidadeMedida $UnidadeMedida
 * @property Usuario $Usuario
 * @property SolicitacaoUsuario $SolicitacaoUsuario
 * @property Status $Status
 * @property AgendaDisponivel $AgendaDisponivel
 * @property Usuario $Usuario
 */
class Solicitacao extends AppModel
{

/**
 * Use table
 *
 * @var mixed False or table name
 */
    public $useTable = 'solicitacao';

/**
 * Validation rules
 *
 * @var array
 */
    public $validate = array(
        'tipo_material_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'unidade_medida_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'usuario_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'solicitacao_usuario_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'quantidade' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
    public $hasOne = array(
        'SolicitacaoPonto' => array(
            'className' => 'SolicitacaoPonto',
            'foreignKey' => 'solicitacao_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
        ),
    );

    public $belongsTo = array(

        'TipoMaterial' => array(
            'className' => 'TipoMaterial',
            'foreignKey' => 'tipo_material_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
        ),
        'UnidadeMedida' => array(
            'className' => 'UnidadeMedida',
            'foreignKey' => 'unidade_medida_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
        ),
        'Usuario' => array(
            'className' => 'Usuario',
            'foreignKey' => 'usuario_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
        ),
        'SolicitacaoUsuario' => array(
            'className' => 'SolicitacaoUsuario',
            'foreignKey' => 'solicitacao_usuario_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
        ),
        'Status' => array(
            'className' => 'Status',
            'foreignKey' => 'status_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
        ),
    );

/**
 * hasMany associations
 *
 * @var array
 */
    public $hasMany = array(
        'AgendaDisponivel' => array(
            'className' => 'AgendaDisponivel',
            'foreignKey' => 'solicitacao_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => '',
        ),
    );

/**
 * hasAndBelongsToMany associations
 *
 * @var array
 */
    public $hasAndBelongsToMany = array(
        'Usuario' => array(
            'className' => 'Usuario',
            'joinTable' => 'solicitacao_usuario',
            'foreignKey' => 'id',
            'associationForeignKey' => 'usuario_id',
            'unique' => 'keepExisting',
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'finderQuery' => '',
        ),
    );

    public function afterFind($results, $primary = false)
    {
        foreach ($results as $indice => $valor) {
            if (isset($valor['Solicitacao']['create'])) {
                $results[$indice]['Solicitacao']['create'] = date('d/m/Y', strtotime($valor['Solicitacao']['create']));
            }
        }
        return $results;
    }

    public function beforeFind($query = array())
    {

        $query['order'] = array('Solicitacao.create DESC');
        return $query;
    }

    public function beforeSave($options = array())
    {
        $this->data['Solicitacao']['create'] = date('Y-m-d H:i:s');

        return true;
    }

}
