<?php
App::uses('AppModel', 'Model');
/**
 * TipoMaterialValor Model
 *
 * @property TipoMaterial $TipoMaterial
 */
class TipoMaterialValor extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'tipo_material_valor';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'tipo_material_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'valor_kilo_litro' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'TipoMaterial' => array(
			'className' => 'TipoMaterial',
			'foreignKey' => 'tipo_material_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
