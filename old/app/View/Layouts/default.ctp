<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en"> <!--<![endif]-->
<head>
    <title>Coletor online -Você ajudando o mundo</title>

    <!-- Meta -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <!-- CSS Global Compulsory-->
    <link rel="stylesheet" href="/plugins/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="/css/style.css">
    <link rel="stylesheet" href="/css/headers/header1.css">
    <link rel="stylesheet" href="/css/responsive.css">
    <link rel="shortcut icon" href="favicon.ico">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
    <?php echo $this->fetch('css');?>


</head>
<body>

<header>

    <?php echo $this->element('menu_off');?>

</header>

<aside>
    <?php echo $this->fetch('slider');?>
</aside>

<section>

<?php if(isset($breadcrumb)){ ?>
<div class="breadcrumbs margin-bottom-40">
    <div class="container">
        <h1 class="pull-left"><?php echo ucfirst($breadcrumb['titulo']);?></h1>
        <ul class="pull-right breadcrumb">
            <?php foreach($breadcrumb['links'] as $link=>$titulo){?>
            <li>
                <a  href="<?php echo $link;?>">
                    <?php echo ucfirst($titulo);?>
                </a>
            </li>
            <?php } ?>
            <li class="active">
            <?php echo ucfirst($breadcrumb['titulo']);?>
            </li>

        </ul>
    </div>
</div>
<?php }?>


    <div class="container" style="padding-top:20px;min-height:600px;" ng-app="myApp">
        <div class="row">
            <?php

            echo $this->Session->flash();
            echo $this->fetch('content');

            ?>
        </div>
    </div>
</section>

<footer>
    <?php echo $this->element('footer')?>

</footer>


 <!-- JS Global Compulsory -->
    <script type="text/javascript" src="/plugins/jquery-1.10.2.min.js"></script>
    <script type="text/javascript" src="/plugins/jquery-migrate-1.2.1.min.js"></script>
    <script type="text/javascript" src="/plugins/bootstrap/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.4.5/angular.min.js"></script>

    <script type="text/javascript" src="/plugins/hover-dropdown.min.js"></script>
    <script type="text/javascript" src="/plugins/back-to-top.js"></script>

    <?php echo $this->fetch('script');?>

    <?php echo $this->fetch('angular');?>

    <script type="text/javascript">
        var app = angular.module('myApp', []);
        app.directive('errSrc', function() {
          return {
                link: function(scope, element, attrs) {

                scope.$watch(function() {
                      return attrs['ngSrc'];
                  }, function (value) {
                      if (!value) {
                        element.attr('src', attrs.errSrc);
                    }
                });

                element.bind('error', function() {
                    element.attr('src', attrs.errSrc);
                });
              }
          }
        });
</script>
</body>
