<?php echo $this->element('sidebar-painel');?>

<div class="col-md-9">
            <!--Basic Table-->
            <div class="panel panel-green margin-bottom-40">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="icon-tasks"></i> Minha participação</h3>
                </div>
                <div class="panel-body">
                    <p>
                    <a href="/solicitacao" class="btn btn-primary pull-right">Nova solicitação</a>
                    </p>
                </div>
                <table class="table">
                    <thead>
                        <tr>
                            <th>Código</th>
                            <th>Data</th>
                            <th class="hidden-sm">Tipo de material</th>
                            <th>Pontuação conquistada</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                     <tbody>

                    <?php foreach($solicitacoes as $indice=>$arrValor){?>
                        <tr>
                            <td><?php echo $arrValor['Solicitacao']['id']; ?></td>
                            <td><?php echo $arrValor['Solicitacao']['create']; ?></td>
                            <td class="hidden-sm"><?php echo $arrValor['TipoMaterial']['descricao']; ?></td>
                            <td><?php echo $arrValor['SolicitacaoPonto']['ponto']; ?></td>
                            <td><?php echo $arrValor['Status']['descricao']; ?></td>
                        </tr>
                    <?php };?>
                    </tbody>
                </table>

            </div>
            <?php
                echo $this->Html->link(
                    'Ver todos',
                    array(
                        'controller' => 'Painel',
                        'action' => 'solicitacoesUsuario',
                        'full_base' => true
                    )
                );
            ?>
            <!--End Basic Table-->




            <div class="margin-bottom-60"></div>

            <div class="headline"><h2>Minha pontuação</h2></div>
            <div class="alert alert-warning fade in">
                <button data-dismiss="alert" class="close" type="button">×</button>
                <strong>Aqui você pode companhar toda pontuação acumulada.
            </div>


            <div class="magazine-sb-categories">
                <div class="row">
                    <ul class="list-unstyled col-xs-6">
                        <li><a href="#">Total de pontos pendentes</a></li>
                        <li><a href="#">Total de pontos confirmados</a></li>

                    </ul>
                    <ul class="list-unstyled col-xs-6">
                        <li><a href="#"><?php echo $totalPontosPendentes;?> pts</a></li>
                        <li><a href="#">0 pts</a></li>

                    </ul>
                </div>
            </div>

</div>