<?php echo $this->element('sidebar-painel');?>
<div class="col-md-9">
    <div class="panel panel-green margin-bottom-40">
        <div class="panel-heading">
            <h3 class="panel-title"><i class="icon-tasks"></i> Minhas solicitações</h3>
        </div>
        <div class="panel-body">
            <p>
                <a href="/solicitacao" class="btn btn-primary pull-right">Nova solicitação</a>
            </p>
        </div>
        <table class="table">
            <thead>
                <tr>
                    <th>Código</th>
                    <th>Data</th>
                    <th class="hidden-sm">Tipo de material</th>
                    <th>Pontuação conquistada</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>

                <?php foreach($solicitacoes as $indice=>$arrValor){?>
                <tr>
                    <td><?php echo $arrValor['Solicitacao']['id']; ?></td>
                    <td><?php echo $arrValor['Solicitacao']['create']; ?></td>
                    <td class="hidden-sm"><?php echo $arrValor['TipoMaterial']['descricao']; ?></td>
                    <td></td>
                    <td><?php echo $arrValor['Status']['descricao']; ?></td>
                </tr>
                <?php };?>
            </tbody>

        </table>




        <div class="text-center">
            <ul class="pagination">
                <?php
                echo $this->Paginator->numbers(array('separator' => '','currentTag' => 'a', 'currentClass' => 'active','tag' => 'li','first' => 1));

                ?>
            </ul>
        </div>

    </div>

</div>