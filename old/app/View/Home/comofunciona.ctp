<ul class="dropdown-menu">
    <li><a href="/como-funciona#projeto">O Projeto</a></li>
    <li><a href="/como-funciona#solicitacao">A Solicitação de coleta</a></li>
    <li><a href="/como-funciona#coleta">A Coleta residêncial</a></li>
    <li><a href="/como-funciona#premio">Os Prêmios de incentivo </a></li>

</ul>

<div id="projeto">
    <h3>O que é o projeto</h3>
    <p>
        O projeto <strong>Coletor online</strong> tem como objetivo incentivar a população a participar mais ativamente do processo de reciclagem de lixo e residuos sólidos.
    </p>
    <p>
        Nosso principal objetivo é que você leitor e internauta possa colaborar ativamente com o processo de reciclagem na sua cidade. Sabemos que tudo começa com pequenas ações e visando incentiva-lo a realizar essas pequenas ações, o <strong>coletor online</strong> é uma ferramenta onde você através da internet solicita a remoção de residuos sólidos
        e a cada solicitação recebe pontos chamados em nosso sistema de eco-pontos, esses pontos podem ser trocados por brindes disponibilizados em nosso website ou concorrer a sorteios que serão realizados pelo website.
    </p>

</div>
<div id="solicitacao">
    <h3>A solicitação</h3>
    <p>
        O processo para solicitar a coleta é bem simples, você deve se cadatrar em nosso website, informar qual tipo de material você tem para coleta (madeira, plastico, vidro, lixo eletronico, etc) depois você escolhe o melhor dia e horário para ter o material removido de sua residência.
    </p>
</div>
<div id="coleta">
    <h3>A coleta</h3>
    <p>
        O processo de coleta funcionará da seguinte forma, após efetuar a solicitação de coleta, o sistema irá gerar uma lista que será enviada as cooperativas de catadores de residuos sólidos mais próximas a sua região, essa cooperativa será responsável por realizar a coleta
    </p>
</div>
<div id="premio">
    <h3>Premiações</h3>
    <p>
        Cada vez que você solicita a coleta de residuos através de coletor online, você recebe uma pontuação o acumulo de ponto possibilia a participação nos sorteios, quando mais pontos você acumular mais chances tem que ganhar.
    </p>
    <p>
        Você tambem pode utilizar seus pontos para efetuar trocas por produtos disponibilizados por nossos parceiros dentro do próprio website.
    </p>
</div>

<div id="duvidas" class="col-md-12">
<div class="row">
    <div class="col-md-4">
    Quem é responsável pela coleta?
    </div>

    <div class="col-md-8">
        A empresa ou pessoa que imprimir primeiro a lista com endereços, as empresas coletoras terão acesso ao sistema em uma área restrita, lá eles irão visualizar a demanda. Imprimir a lista reservar a coleta para aquela empresa e então pegar o material.
    </div>
</div>
</div>