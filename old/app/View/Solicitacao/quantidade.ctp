<?php echo $this->element('sidebar-painel');?>
<?php $this->start('script');?>
<script type="text/javascript">


    function get_location() {
      navigator.geolocation.getCurrentPosition(show_map);
    }

    function show_map(position) {
      var latitude = position.coords.latitude;
      var longitude = position.coords.longitude;
      // let's show a map or do something interesting!

    }
    $(document).ready(function(){

    })
</script>
<?php $this->end();?>
<div class="col-md-9">

    <div class="margin-bottom-10"></div>

    <div class="headline"><h2>Quantidade de cada material</h2></div>

    <div class="alert alert-danger">
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      <strong>Atenção!</strong> Essa informação é importante para que a empresa ou responsável que irá coletar o material saiba qual é o melhor veículo ou meio de remover o material do endereço indicado.
    </div>


    <form action="/solicitacao/agenda" method="post">
        <?php foreach($arrSolicitacao['Solicitacao'] as $index=>$arrDados){?>
        <div class="row">
            <div class="col-sm-2">
                <?php echo $arrDados['descricao'];?>
                <?php echo $this->Form->input('Solicitacao.id',
                array(
                'type'=>'hidden',
                'value'=>$arrDados['tipo_material_id'],
                'name'=>'data[Solicitacao]['.$index.'][tipo_material_id]'
                )
                );?>
            </div>
            <div class="col-sm-6">

                <div class="input-group">
                    <?php echo $this->Form->input('Solicitacao.quantidade',
                    array(
                    'label'=>false,
                    'class'=>'form-control',
                    'placeholder'=>"Quantidade média",
                    'name'=>'data[Solicitacao]['.$index.'][quantidade]'
                    ));?>
                    <span class="input-group-addon">
                        <i
                        class="fa fa-info-circle"
                        data-container="body"
                        data-toggle="popover"
                        data-placement="top"
                        tabindex="0"
                        role="button"
                        data-trigger="focus"
                        data-content="Informe aqui a quantidade média de peso, não é necessário que seja a quantidade exata porém o mais próximo ao real, isso irá será validado quando o coletor vier remover o material."></i>
                    </span>
                </div>



            </div>
            <div class="col-sm-4">
                <?php echo $this->Form->input('Solicitacao.unidade_medida_id',
                array(
                'label'=>false,
                'options'=>$unidadeMedida,
                'class'=>'form-control',
                'name'=>'data[Solicitacao]['.$index.'][unidade_medida_id]')
                );?>
            </div>
        </div>
        <br/>

        <?php }?>
<br/><br/>
        <div class="row text-right">
        <button type="submit" class="btn btn-u btn-lg">Próximo</button>
        </div>
    </form>

</div>

<?php $this->start('script');?>
<script type="text/javascript">
    $('[data-toggle="popover"]').popover()
</script>
<?php $this->end();?>