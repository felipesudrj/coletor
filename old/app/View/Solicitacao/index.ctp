<?php echo $this->element('sidebar-painel');?>

<div ng-app="application" ng-controller="controllercadastro" class="col-md-9">

    <div class="margin-bottom-10"></div>

    <div class="headline"><h2>Selecione o tipo de material</h2></div>
    <form action="/solicitacao/quantidade" method="post">

        <div class="row servive-block margin-bottom-10">
            <?php foreach($tipoMaterial as $arrValor){ ?>
            <div class="col-md-3 col-sm-6">
                <div class="servive-block-in servive-block-colored <?php echo $arrValor['TipoMaterial']['bgcolor'] ?>">
                    <h4><?php echo $arrValor['TipoMaterial']['descricao'] ?></h4>
                    <div><i class="<?php echo $arrValor['TipoMaterial']['icone'] ?>"></i></div>
                    <p class="text-center">
                        <input
                        ng-click="pegarMaterial('<?php echo $arrValor['TipoMaterial']['id'] ?>',this)"
                        type="checkbox"
                        name="data[tipoMaterial][id][]"
                        value="<?php echo $arrValor['TipoMaterial']['id'] ?>"
                        >
                    </p>
                </div>
            </div>
            <?php } ?>

        </div>
         <div class="text-right">
        <button type="submit" class="btn btn-u btn-lg">Próximo</button>
        </div>
    </form>
</div>


<?php $this->start('angular'); ?>

<script>
    var app = angular.module('application', []);
    app.controller('controllercadastro', function($scope) {


    });
</script>

<?php $this->end(); ?>