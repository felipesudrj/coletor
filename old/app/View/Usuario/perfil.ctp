<?php $this->start('css');?>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.11.4/themes/south-street/jquery-ui.css">
<style type="text/css">
    #map{
        height: 350px;
        width: 100%;
    }
</style>
<?php $this->end();?>
<?php $this->start('script');?>
<script type="text/javascript" src="https://code.jquery.com/ui/1.11.4/jquery-ui.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.13.4/jquery.mask.min.js"></script>
<script src="/js/ui.datepicker-pt-BR.js"></script>

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>

<script type="text/javascript" src="/js/gmaps.js"></script>




<script type="text/javascript">

    function buscarApi(cep){
        cep = $('#cep').val().trim();
        $.ajax({
            url:'http://api.postmon.com.br/v1/cep/'+cep,
            dataType:'json',
            beforeSend: function($xrH){
                //mesage buscanco endereco
            },
            success:function(data){
                if(data != null){

                    $('#endereco').val(data.logradouro);
                    $('#bairro').val(data.bairro);
                    $('#cidade').val(data.cidade);
                    $('#endereco_id').val('');
                    $('#estado :selected').val()
                    var enderecoCompleto = $('#endereco').val()+" "+
                    $('#numero').val()+" "+
                    $('#cidade').val()+" "+
                    $('#estado :selected').text();
                    return true;
                }
                return false;
            },
            error:function(){

            }

        })
    }

    function buscarEndereco(){
        cep = $('#cep').val().trim();
        $.ajax({
            url:'/enderecos/buscar/'+cep,
            dataType:'json',
            beforeSend: function($xrH){
                //mesage buscanco endereco
            },
            success:function(data){
                if(data != null){

                    $('#endereco').val(data.Endereco.logradouro);
                    $('#bairro').val(data.Bairro.descricao);
                    $('#cidade').val(data.Bairro.Cidade.descricao);
                    $('#estado').val(data.Bairro.Cidade.estado_id);
                    $('#endereco_id').val(data.Endereco.endereco_id);
                    var enderecoCompleto = $('#endereco').val()+" "+
                    $('#numero').val()+" "+
                    $('#cidade').val()+" "+
                    $('#estado :selected').text();
                    return true;
                }else{
                  buscarApi(cep)
                }



                return false;
            },
            error:function(){

            }

        })
    }

    var map;
    var latitude = '<?php echo $this->request->data['Usuario']['latitude'];?>';
    var longitude = '<?php echo $this->request->data['Usuario']['longitude'];?>';
    $(document).ready(function(){

        $('.endereco').attr('readonly', true);

        $('.data').datepicker(
            {changeYear: true,
            yearRange: "1960:2016",
            changeMonth: true
        });
        $('.telefone').mask('(99)9999-99999');

        $('#cep').mask('99999999');
        map = new GMaps({
            el: '#map',
            lat: latitude,
            lng: longitude
        });


        $('.cep').change(function(e){
            e.preventDefault();
            var enderecoCompleto = buscarEndereco();
            console.log(enderecoCompleto);

            GMaps.geocode({
              address: $('#cep').val().trim()/*$('#cep').val().trim()*/,
              callback: function(results, status){
                if(status=='OK'){
                  var endereco = results[0];
                  var latlng = results[0].geometry.location;
                  map.setCenter(latlng.lat(), latlng.lng());

                  $('#latitude').val(latlng.lat());
                  $('#longitude').val(latlng.lng());

                  map.addMarker({
                    lat: latlng.lat(),
                    lng: latlng.lng()
                });
              }
          }
      });
        });
    })
</script>
<?php $this->end();?>



<div class="panel panel-green margin-bottom-40">
    <div class="panel-heading">
        <h3 class="panel-title"><i class="icon-tasks"></i> <?php echo ucfirst(h('Atualização de perfil'));?></h3>
    </div>
    <div class="panel-body">
    <form action="/usuario/perfil" method="post" class="form-horizontal">
        <div class="form-group">
            <label for="inputEmail1" class="col-lg-2 control-label"><?php echo ucfirst(h('nome'));?></label>
            <div class="col-lg-10">
                <?php echo $this->Form->input('Usuario.nome',array('label'=>false,'div'=>false,'class'=>'form-control'));   ?>
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail1" class="col-lg-2 control-label"><?php echo ucfirst(h('e-mail'));?></label>
            <div class="col-lg-10">
                <?php echo $this->Form->input('User.username',array('label'=>false,'div'=>false,'class'=>'form-control'));   ?>
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail1" class="col-lg-2 control-label"><?php echo ucfirst(h('Data de nascimento'));?></label>
            <div class="col-lg-10">
                <?php echo $this->Form->input('Usuario.data_nascimento',array('type'=>'text','label'=>false,'div'=>false,'class'=>'data form-control'));   ?>
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail1" class="col-lg-2 control-label"><?php echo ucfirst(h('Telefone'));?></label>
            <div class="col-lg-10">
                <?php echo $this->Form->input('Usuario.telefone',array('type'=>'text','label'=>false,'div'=>false,'class'=>'telefone form-control'));   ?>
            </div>
        </div>
        <hr/>

        <div class="form-group">
            <label for="inputEmail1" class="col-lg-2 control-label"><?php echo ucfirst(h('CEP'));?></label>
            <div class="col-lg-10">
                <?php echo $this->Form->input('Usuario.cep',array('type'=>'text','label'=>false,'div'=>false,'id'=>'cep','class'=>'cep form-control'));   ?>
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail1" class="col-lg-2 control-label"><?php echo ucfirst(h('endereco'));?></label>
            <div class="col-lg-10">
                <?php echo $this->Form->input('Usuario.endereco',array('label'=>false,'div'=>false,'id'=>'endereco','class'=>'endereco form-control margin-bottom-20'));?>
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail1" class="col-lg-2 control-label"><?php echo ucfirst(h('número'));?></label>
            <div class="col-lg-10">
                <?php echo $this->Form->input('Usuario.numero',array('label'=>false,'div'=>false,'id'=>'numero','class'=>'cep form-control margin-bottom-20'));?>
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail1" class="col-lg-2 control-label"><?php echo ucfirst(h('complemento'));?></label>
            <div class="col-lg-10">
            <?php echo $this->Form->input('Usuario.complemento',array('label'=>false,'div'=>false,'id'=>'complemento','class'=>'form-control margin-bottom-20'));?></div>
        </div>
        <div class="form-group">
            <label for="inputEmail1" class="col-lg-2 control-label"><?php echo ucfirst(h('endereco_id'));?></label>
            <div class="col-lg-10">
                <?php echo $this->Form->input('Usuario.endereco_id',array('type'=>'hidden','label'=>false,'div'=>false,'id'=>'endereco_id'));?>
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail1" class="col-lg-2 control-label"><?php echo ucfirst(h('bairro'));?></label>
            <div class="col-lg-10">
                <?php echo $this->Form->input('Usuario.bairro',array('label'=>false,'div'=>false,'id'=>'bairro','class'=>'endereco form-control margin-bottom-20'));?>
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail1" class="col-lg-2 control-label"><?php echo ucfirst(h('cidade'));?></label>
            <div class="col-lg-10">
                <?php echo $this->Form->input('Usuario.cidade',array('label'=>false,'div'=>false,'id'=>'cidade','class'=>'endereco form-control margin-bottom-20'));?>
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail1" class="col-lg-2 control-label"><?php echo ucfirst(h('estado'));?></label>
            <div class="col-lg-10">
            <?php echo $this->Form->input('Usuario.estado',array('options'=>$estados,'label'=>false,'div'=>false,'id'=>'estado','class'=>'endereco form-control margin-bottom-20'));?></div>
        </div>

        <div class="form-group">
            <div class="col-lg-offset-2 col-lg-10">
            <div id="map"></div>
            </div>
        </div>


        <div class="form-group">
            <div class="col-lg-offset-2 col-lg-10">


<div class="row-fluid">
                <div class="col-sm-6">
                    <label>Latitude <span class="color-red">*</span></label>
                    <?php echo $this->Form->input('Usuario.latitude',array('label'=>false,'div'=>false,'id'=>'latitude','class'=>'endereco form-control margin-bottom-20'));?>
                </div>
                <div class="col-sm-6">
                    <label>Longitude <span class="color-red">*</span></label>
                    <?php echo $this->Form->input('Usuario.longitude',array('label'=>false,'div'=>false,'id'=>'longitude','class'=>'endereco form-control margin-bottom-20'));?>
                </div>
</div>


            </div>
        </div>





        <div class="form-group">
            <div class="col-lg-offset-10 col-lg-2">
                <?php echo $this->Form->submit('Salvar',array('class'=>'pull-right btn-u btn-u-green'));?>

            </div>
        </div>
        </form>
    </div>
</div>