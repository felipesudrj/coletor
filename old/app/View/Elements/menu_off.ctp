    <!--=== Top ===-->
   <?php
    $ssessao = $this->Session->read('Auth.User');
    ?>
    <div class="top">
        <div class="container">
            <ul class="loginbar pull-right">
                <?php if(!isset($ssessao)){ ?>
                <li>
                    <i class="icon-globe"></i>
                    <a>Languages</a>
                    <ul class="lenguages">
                        <li class="active">
                            <a href="#">Português <i class="icon-ok"></i></a>
                        </li>
                        <li><a href="#">English</a></li>
                        <li><a href="#">Spanish</a></li>
                        <li><a href="#">Russian</a></li>
                        <li><a href="#">German</a></li>
                    </ul>
                </li>
                <li class="devider"></li>
                <li><a href="/login"><span class="badge">Login</span></a></li>
                <li class="devider"></li>
                <li><a href="/cadastro"><span class="badge">Cadastre-se</span></a></a></li>
                <?php  }else{?>
                    <li><a href="/painel"><span class="badge badge-danger">Painel</span></a></li>
                                    <li class="devider"></li>

                    <li><a href="/usuario/perfil"><span class="badge badge-primary">Perfil</span></a></li>
                <li class="devider"></li>

                    <li><a href="/user/logout"><span class="badge badge-warning">Sair</span></a></li>
                <?php }?>
            </ul>
        </div>
    </div><!--/top-->
    <!--=== End Top ===-->

<nav>
    <!--=== Header ===-->
    <div class="header">
        <div class="navbar navbar-default" role="navigation">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                        <span class="sr-only">Barra de navegação</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/">
                        <img id="logo-header" src="/img/logo.png" alt="Logo">
                    </a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-responsive-collapse">
                    <ul class="nav navbar-nav navbar-right">
                        <li class=" active">
                            <a href="/" >
                                Home
                            </a>

                        </li>
                        <li class="dropdown">
                            <a href="/como-funciona" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">
                                Como funciona
                                <i class="icon-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="/como-funciona#projeto">O Projeto</a></li>
                                <li><a href="/como-funciona#solicitacao">A Solicitação de coleta</a></li>
                                <li><a href="/como-funciona#coleta">A Coleta residêncial</a></li>
                                <li><a href="/como-funciona#premio">Os Prêmios de incentivo </a></li>

                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">
                                Parceiros
                                <i class="icon-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="/parceiros#empresas">Empresas parceiras</a></li>
                                <li><a href="/parceiros#apoiadores">Apoiadores</a></li>
                                <li><a href="/parceiros#governo">Orgãos governamentais</a></li>

                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">
                                Prêmios
                                <i class="icon-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="/home/ranking">Ranking das cidades</a></li>
                                <li><a href="/home/premios">Prêmios de incentivo disponíveis</a></li>

                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">
                                Blog
                                <i class="icon-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="#">Coleta Seletiva</a></li>
                                <li><a href="#">Transformação do lixo</a></li>
                                <li><a href="#">Alerta Verde</a></li>
                                <li><a href="#">Política de resíduos sólidos</a></li>
                                <li><a href="#">Veja todos os tópicos</a></li>

                            </ul>
                        </li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-delay="0" data-close-others="false">
                                Contato

                            </a>
                            <ul class="dropdown-menu">
                                <li><a href="page_contact1.html">Canal Verde - fale conosco</a></li>
                                <li><a href="page_contact2.html">Seja um eco-parceiro</a></li>
                                <li><a href="page_contact3.html">FAQ</a></li>
                            </ul>
                        </li>
                        <li class="hidden-sm"><a class="search"><i class="icon-search search-btn"></i></a></li>
                    </ul>
                    <div class="search-open">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search">
                            <span class="input-group-btn">
                                <button class="btn-u" type="button">Go</button>
                            </span>
                        </div><!-- /input-group -->
                    </div>
                </div><!-- /navbar-collapse -->
            </div>
        </div>
    </div><!--/header-->

</nav>
<!--=== End Header ===-->
