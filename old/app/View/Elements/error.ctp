<div class="alert alert-danger">
    <i class="fa fa-frown-o fa-3x"></i> <?php echo h($message); ?>
</div>
