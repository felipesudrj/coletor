<div class="col-md-3">

            <div class="margin-bottom-30">
                            <div class="headline"><h2><?php echo h('Seu Perfil')?></h2></div>
                             <div class="perfil">
                                <img ng-src="{{imgsrc}}" err-src="/img/user.jpg" ng-init="imgsrc='/assets/img/testimonials/img1.jpg'" alt="" class="col-md-4 img-recursive img-circle ">
                                <span class="dados-pessoais">
                                    <?php echo $this->Session->read('Auth.User.Usuario.nome');?>
                                    <br/>
                                    <em>Último acesso</em>
                                    <br/>
                                    <strong>Saldo de pontos: <?php echo $totalPontosConfirmados;?></strong>
                                    <br/>

                                    </span>

                            </div>
                            <br/>
            </div>


            <!-- About Us -->
            <div class="margin-bottom-30">
                <div class="headline"><h2><?php echo h('Últimas notícias')?></h2></div>
                <p>
                <?php echo h('Lorem ipsum Exercitation irure Duis voluptate commodo Duis sit sit in dolore nisi in eiusmod enim dolor id dolor Excepteur esse sint enim enim laborum in.');?>
                </p>
                <ul class="list-unstyled">
                    <li><i class="fa fa-check color-green"></i> Lorem ipsum Anim amet do cillum nulla esse.</li>
                    <li><i class="fa fa-check color-green"></i> Lorem ipsum Labore ex pariatur eiusmod in proident.</li>
                    <li><i class="fa fa-check color-green"></i> Lorem ipsum Est Ut fugiat ullamco.</li>
                    <li><i class="fa fa-check color-green"></i> Lorem ipsum Irure in aliquip consequat tempor.</li>
                </ul>
            </div>

            <!-- Posts -->
            <div class="posts margin-bottom-30">
                <div class="headline"><h2><?php echo h('Últimos ganhadores'); ?></h2></div>
                <dl class="dl-horizontal">
                    <dt><a href="#"><img src="/assets/img/sliders/elastislide/10.jpg" alt=""></a></dt>
                    <dd>
                        <p><a href="#">Lorem sequat ipsum dolor lorem sunt aliqua put</a></p>
                    </dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt><a href="#"><img src="/assets/img/sliders/elastislide/11.jpg" alt=""></a></dt>
                    <dd>
                        <p><a href="#">It works on all major web browsers tablets</a></p>
                    </dd>
                </dl>
                <dl class="dl-horizontal">
                    <dt><a href="#"><img src="/assets/img/sliders/elastislide/9.jpg" alt=""></a></dt>
                    <dd>
                        <p><a href="#">Brunch 3 wolf moon tempor sunt aliqua put.</a></p>
                    </dd>
                </dl>
            </div><!--/posts-->

            <!-- Contact Us -->
            <div class="who margin-bottom-30">
                <div class="headline"><h2>Cooperativas</h2></div>
                <p>Vero facilis est etenim a feugiat cupiditate non quos etrerum facilis.</p>
                <ul class="list-unstyled">
                    <li><a href="#"><i class="icon-home"></i>5B amus ED554, New York, US</a></li>
                    <li><a href="#"><i class="icon-envelope-alt"></i>infp@example.com</a></li>
                    <li><a href="#"><i class="icon-phone-sign"></i>1(222) 5x86 x97x</a></li>
                    <li><a href="#"><i class="icon-globe"></i>http://www.example.com</a></li>
                </ul>
            </div>
        </div>