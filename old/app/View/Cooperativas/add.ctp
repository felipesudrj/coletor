


<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 cooperativas">
    <div class="panel panel-green margin-bottom-40">

            <div class="panel-heading">
                <h3 class="panel-title"><i class="icon-tasks"></i> <?php echo __('Nova cooperativa'); ?></h3>
            </div>
            <div class="panel-body">

			     <?php echo $this->Form->create('Cooperativa', array('role' => 'form','class'=>'form-vertical')); ?>

				<div class="form-group">
					<?php echo $this->Form->input('nome', array('class' => 'form-control', 'placeholder' => 'Nome'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('endereco_id', array('class' => 'form-control', 'placeholder' => 'Endereco Id'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('telefone', array('class' => 'form-control', 'placeholder' => 'Telefone'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->input('email', array('class' => 'form-control', 'placeholder' => 'Email'));?>
				</div>
				<div class="form-group">
					<?php echo $this->Form->submit(__('Salvar'), array('class' => 'btn btn-u btn-u-green')); ?>
				</div>

			<?php echo $this->Form->end() ?>

        </div>
    </div>
</div>
