<?php echo $this->element('sidebar-painel');?>


<div class="col-md-9 cooperativas index">
    <div class="headline"><h2><?php echo __('Cooperativas'); ?></h2></div>


	<div class="row">



			<table cellpadding="0" cellspacing="0" class="table table-striped">
				<thead>
					<tr>
						<th><?php echo $this->Paginator->sort('id'); ?></th>
						<th><?php echo $this->Paginator->sort('nome'); ?></th>
						<th><?php echo $this->Paginator->sort('endereco_id'); ?></th>
						<th><?php echo $this->Paginator->sort('telefone'); ?></th>
						<th><?php echo $this->Paginator->sort('email'); ?></th>
						<th class="actions"></th>
					</tr>
				</thead>
				<tbody>
				<?php foreach ($cooperativas as $cooperativa): ?>
					<tr>
						<td><?php echo h($cooperativa['Cooperativa']['id']); ?>&nbsp;</td>
						<td><?php echo h($cooperativa['Cooperativa']['nome']); ?>&nbsp;</td>
								<td>
			<?php echo $this->Html->link($cooperativa['Endereco']['endereco_id'], array('controller' => 'enderecos', 'action' => 'view', $cooperativa['Endereco']['endereco_id'])); ?>
		</td>
						<td><?php echo h($cooperativa['Cooperativa']['telefone']); ?>&nbsp;</td>
						<td><?php echo h($cooperativa['Cooperativa']['email']); ?>&nbsp;</td>
						<td class="actions">
							<?php echo $this->Html->link('<span class="glyphicon glyphicon-search"></span>', array('action' => 'view', $cooperativa['Cooperativa']['id']), array('escape' => false)); ?>
							<?php echo $this->Html->link('<span class="glyphicon glyphicon-edit"></span>', array('action' => 'edit', $cooperativa['Cooperativa']['id']), array('escape' => false)); ?>
							<?php echo $this->Form->postLink('<span class="glyphicon glyphicon-remove"></span>', array('action' => 'delete', $cooperativa['Cooperativa']['id']), array('escape' => false), __('Tem certeza que deseja excluir # %s?', $cooperativa['Cooperativa']['id'])); ?>
						</td>
					</tr>
				<?php endforeach; ?>
				</tbody>
			</table>

			<p>
				<small><?php echo $this->Paginator->counter(array('format' => __('Página {:page} de {:pages}, Mostrando {:current} registro dos {:count} encontrados, Iniciando os registros em {:start}, terminando em {:end}')));?></small>
			</p>

			<?php
			$params = $this->Paginator->params();
			if ($params['pageCount'] > 1) {
			?>
			<ul class="pagination pagination-sm">
				<?php
					echo $this->Paginator->prev('&larr; Próximo', array('class' => 'prev','tag' => 'li','escape' => false), '<a onclick="return false;">&larr; Próximo</a>', array('class' => 'prev disabled','tag' => 'li','escape' => false));
					echo $this->Paginator->numbers(array('separator' => '','tag' => 'li','currentClass' => 'active','currentTag' => 'a'));
					echo $this->Paginator->next('Anterior &rarr;', array('class' => 'next','tag' => 'li','escape' => false), '<a onclick="return false;">Próximo &rarr;</a>', array('class' => 'next disabled','tag' => 'li','escape' => false));
				?>
			</ul>
			<?php } ?>


	</div><!-- end row -->


</div><!-- end containing of content -->