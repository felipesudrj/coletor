<div class="row">
        <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
            <form method="post" action="/login" class="reg-page">

                <div class="reg-header">
                    <h2><?php echo h('Realizar login');?></h2>
                </div>

                <div class="input-group margin-bottom-20">
                    <span class="input-group-addon"><i class="fa fa-envelope-o"></i> Email</span>
                    <?php echo $this->Form->input('User.username',array('type'=>'email','class'=>'form-control','div'=>false,'label'=>false,'placeholder'=>'Email'));?>

                </div>
                <div class="input-group margin-bottom-20">
                    <span class="input-group-addon"><i class="fa fa-lock"></i> Senha</span>
                    <?php echo $this->Form->input('User.password',array('placeholder'=>'Senha','class'=>'form-control','div'=>false,'label'=>false));?>
                </div>

                <div class="row">
                    <div class="col-md-6">
                    </div>
                    <div class="col-md-6">
                        <?php echo $this->Form->submit('Entrar',array('class'=>'btn-u pull-right')); ?>
                    </div>
                </div>

                <hr>

                <h4><?php echo h('Esqueceu sua senha?');?></h4>
                <p><?php echo h('Sem problema');?>, <a class="color-green" href="#"><?php echo h('Clique aqui');?></a> <?php echo h('para que possamos te reenviar');?>.</p>
            </form>
        </div>
    </div><!--/row-->