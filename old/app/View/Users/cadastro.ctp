<?php $this->start('css');?>
<style type="text/css">
    #map{
        height: 350px;
        width: 100%;
    }
</style>
<?php $this->end();?>

<?php $this->start('script');?>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js"></script>
<script type="text/javascript"></script>
<script type="text/javascript" src="/js/gmaps.js"></script>
<script type="text/javascript">
    function buscarEndereco(){
        cep = $('#cep').val().trim();
        $.ajax({
            url:'/enderecos/buscar/'+cep,
            dataType:'json',
            beforeSend: function($xrH){
                //mesage buscanco endereco
            },
            success:function(data){
                if(data != null){

                    $('#endereco').val(data.Endereco.logradouro);
                    $('#bairro').val(data.Bairro.descricao);
                    $('#cidade').val(data.Bairro.Cidade.descricao);
                    $('#estado').val(data.Bairro.Cidade.estado_id);
                    $('#endereco_id').val(data.Endereco.endereco_id);
                    var enderecoCompleto = $('#endereco').val()+" "+
                    $('#numero').val()+" "+
                    $('#cidade').val()+" "+
                    $('#estado :selected').text();
                    return true;
                }
                return false;
            },
            error:function(){

            }

        })
    }


    var map;
    $(document).ready(function(){

        $('#cep').mask('99999999');
        map = new GMaps({
            el: '#map',
            lat: -12.043333,
            lng: -77.028333
        });



        $('.cep').change(function(e){
            e.preventDefault();
            var enderecoCompleto = buscarEndereco();
            console.log(enderecoCompleto);

            GMaps.geocode({
              address: $('#cep').val().trim()/*$('#cep').val().trim()*/,
              callback: function(results, status){
                if(status=='OK'){
                  var endereco = results[0];
                  var latlng = results[0].geometry.location;
                  map.setCenter(latlng.lat(), latlng.lng());

                  $('#latitude').val(latlng.lat());
                  $('#longitude').val(latlng.lng());

                  map.addMarker({
                    lat: latlng.lat(),
                    lng: latlng.lng()
                });
              }
          }
      });
        });
    });
</script>

<?php $this->end();?>
<div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
    <form action="/cadastro" class="reg-page" method="post">


            <?php if(isset($validador)){?>

            <div class="alert alert-danger">
                  <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                    <?php

                    $this->Form->inputDefaults(array(
                            'error' => false,
                        )
                    );
                    $errors = "";

                    foreach ($validador as $key => $value) {
                        $errors .= $this->Html->tag('li', $value['0']);

                    }

                        echo $this->Html->tag('ul', $errors);

                    ?>

            </div>

            <?php }?>

            <div class="reg-header">
                <h2>Crie sua conta</h2>
                <p>Já possui uma conta? Então <a href="page_login.html" class="color-green"><strong>click aqui</strong></a> para realizar seu login.</p>
            </div>

            <label>Nome <span class="color-red">*</span></label>
            <?php echo $this->Form->input('Usuario.nome',array('label'=>false,'div'=>false,'id'=>'nome','class'=>'form-control margin-bottom-20'));?>

            <label>Email <span class="color-red">*</span></label>
            <?php echo $this->Form->input('User.username',array('label'=>false,'div'=>false,'id'=>'email','class'=>'form-control margin-bottom-20'));?>

            <label>CEP <span class="color-red">*</span></label>
            <?php echo $this->Form->input('Usuario.cep',array('label'=>false,'div'=>false,'id'=>'cep','class'=>'cep form-control margin-bottom-20'));?>

            <label>Endereço <span class="color-red">*</span></label>
            <?php echo $this->Form->input('Usuario.endereco',array('label'=>false,'div'=>false,'id'=>'endereco','class'=>'form-control margin-bottom-20'));?>


            <label>Número <span class="color-red">*</span></label>
            <?php echo $this->Form->input('Usuario.numero',array('label'=>false,'div'=>false,'id'=>'numero','class'=>'cep form-control margin-bottom-20'));?>


            <label>Complemento</label>
            <?php echo $this->Form->input('Usuario.complemento',array('label'=>false,'div'=>false,'id'=>'complemento','class'=>'form-control margin-bottom-20'));?>

            <?php echo $this->Form->input('Usuario.endereco_id',array('type'=>'hidden','label'=>false,'div'=>false,'id'=>'endereco_id'));?>

            <label>Bairro <span class="color-red">*</span></label>
            <?php echo $this->Form->input('Usuario.bairro',array('label'=>false,'div'=>false,'id'=>'bairro','class'=>'form-control margin-bottom-20'));?>

            <label>Cidade <span class="color-red">*</span></label>
            <?php echo $this->Form->input('Usuario.cidade',array('label'=>false,'div'=>false,'id'=>'cidade','class'=>'form-control margin-bottom-20'));?>

            <label>Estado <span class="color-red">*</span></label>
            <?php echo $this->Form->input('Usuario.estado',array('options'=>$estados,'label'=>false,'div'=>false,'id'=>'estado','class'=>'form-control margin-bottom-20'));?>




            <div id="map"></div>

            <div class="row">
                <div class="col-sm-6">
                    <label>Latitude <span class="color-red">*</span></label>
                    <?php echo $this->Form->input('Usuario.latitude',array('label'=>false,'div'=>false,'id'=>'latitude','class'=>'form-control margin-bottom-20'));?>
                </div>
                <div class="col-sm-6">
                    <label>Longitude <span class="color-red">*</span></label>
                    <?php echo $this->Form->input('Usuario.longitude',array('label'=>false,'div'=>false,'id'=>'longitude','class'=>'form-control margin-bottom-20'));?>
                </div>
            </div>


            <div class="row">
                <div class="col-sm-6">
                    <label>Senha <span class="color-red">*</span></label>
                    <?php echo $this->Form->input('User.password',array('type'=>'password','label'=>false,'div'=>false,'id'=>'senha','class'=>'form-control margin-bottom-20'));?>
                </div>
                <div class="col-sm-6">
                    <label>Confirme sua senha <span class="color-red">*</span></label>
                    <?php echo $this->Form->input('User.confirm_password',array('type'=>'password','label'=>false,'div'=>false,'id'=>'confirm_senha','class'=>'form-control margin-bottom-20'));?>                    </div>
                </div>

                <hr>

                <div class="row">
                    <div class="col-lg-6">
                        <label class="checkbox">
                            <input type="checkbox">
                            Eu li os <a href="page_terms.html" class="color-green">Termos, regras e condições de uso</a>
                        </label>
                    </div>
                    <div class="col-lg-6 text-right">
                        <button class="btn-u" type="submit">Confirmar</button>
                    </div>
                </div>
            </form>
        </div>