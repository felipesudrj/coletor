<?php
App::uses('AppModel', 'Model');
/**
 * SolicitacaoUsuario Model
 *
 * @property Usuario $Usuario
 * @property Solicitacao $Solicitacao
 */
class SolicitacaoUsuario extends AppModel
{

/**
 * Use table
 *
 * @var mixed False or table name
 */
    public $useTable = 'solicitacao_usuario';

/**
 * Validation rules
 *
 * @var array
 */
    public $validate = array(
        'ip' => array(
            'ip' => array(
                'rule' => array('ip'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
    public $belongsTo = array(
        'Usuario' => array(
            'className' => 'Usuario',
            'foreignKey' => 'usuario_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
        ),
    );

/**
 * hasMany associations
 *
 * @var array
 */
    public $hasMany = array(
        'Solicitacao' => array(
            'className' => 'Solicitacao',
            'foreignKey' => 'solicitacao_usuario_id',
            'dependent' => false,
            'conditions' => '',
            'fields' => '',
            'order' => '',
            'limit' => '',
            'offset' => '',
            'exclusive' => '',
            'finderQuery' => '',
            'counterQuery' => '',
        ),
    );

    public function beforeSave($options = array())
    {
        $this->data['SolicitacaoUsuario']['create'] = date('Y-m-d H:i:s');

        return true;
    }

    public function beforeFind($query = array())
    {

        $query['order'] = array('SolicitacaoUsuario.create DESC');
        return $query;
    }
}
