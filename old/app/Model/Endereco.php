<?php
App::uses('AppModel', 'Model');
/**
 * Endereco Model
 *
 * @property Bairro $Bairro
 */
class Endereco extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'endereco';

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'endereco_id';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'bairro_id' => array(
			'numeric' => array(
				'rule' => array('numeric'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'cep' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'logradouro' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);

	//The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
	public $belongsTo = array(
		'Bairro' => array(
			'className' => 'Bairro',
			'foreignKey' => 'bairro_id',
			'conditions' => '',
			'fields' => '',
			'order' => ''
		)
	);
}
