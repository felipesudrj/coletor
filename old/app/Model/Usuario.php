<?php
App::uses('AppModel', 'Model');
App::uses('Endereco', 'Model');
App::uses('Cidade', 'Model');

/**
 * Usuario Model
 *
 * @property User $User
 * @property Endereco $Endereco
 * @property Cidade $Endereco
 */
class Usuario extends AppModel
{

    public $useTable = 'usuario';

    public $validate = array(

        'endereco_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                'message' => 'Endereço invalido erro 504547 - entre em contato pelo email contato@coletor.com.br',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'nome' => array(
            'notEmpty' => array(
                'rule' => array('notEmpty'),
                'message' => 'Informe seu nome',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
        ),
        'Endereco' => array(
            'className' => 'Endereco',
            'foreignKey' => 'endereco_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
        ),
    );

    public function beforeSave($options = array())
    {

        if (isset($this->data['Usuario']['data_nascimento'])) {

            $novaData = explode('/', $this->data['Usuario']['data_nascimento']);
            $this->data['Usuario']['data_nascimento'] = $novaData['2'] . '-' . $novaData['1'] . '-' . $novaData['0'];

        }

        $this->set($this->data);

        if (!$this->validates()) {
            die('nao validou');
            return false;
        }

        return true;
    }

    public function afterFind($results, $primary = false)
    {
        foreach ($results as $indice => $valor) {
            if (isset($valor['Usuario']['data_nascimento'])) {
                $results[$indice]['Usuario']['data_nascimento'] = date('d/m/Y', strtotime($valor['Usuario']['data_nascimento']));
            }
        }
        return $results;
    }

    public function getUsuario($usuario_id)
    {

        $arrResultado = array();

        $usuario = $this->findByid($usuario_id);

        $endereco = $this->Endereco->findByendereco_id($usuario['Usuario']['endereco_id']);

        $Cidade = new Cidade();
        $cidade = $Cidade->findBycidade_id($endereco['Bairro']['cidade_id']);

        $usuario['Usuario']['cep'] = $endereco['Endereco']['cep'];
        $usuario['Usuario']['endereco'] = $endereco['Endereco']['logradouro'];
        $usuario['Usuario']['bairro'] = $endereco['Bairro']['descricao'];
        $usuario['Usuario']['cidade'] = $cidade['Cidade']['descricao'];
        $usuario['Usuario']['estado'] = $cidade['Cidade']['estado_id'];

        $arrResultado = array_merge($usuario, $endereco, $cidade);

        return $arrResultado;
    }

}
