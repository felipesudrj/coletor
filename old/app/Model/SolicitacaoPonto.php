<?php
App::uses('AppModel', 'Model');
/**
 * SolicitacaoPonto Model
 *
 * @property Solicitacao $Solicitacao
 * @property Usuario $Usuario
 */
class SolicitacaoPonto extends AppModel
{

/**
 * Use table
 *
 * @var mixed False or table name
 */
    public $useTable = 'solicitacao_ponto';

/**
 * Validation rules
 *
 * @var array
 */
    public $validate = array(
        'solicitacao_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'usuario_id' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'ponto' => array(
            'numeric' => array(
                'rule' => array('numeric'),
                //'message' => 'Your custom message here',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                //'on' => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
    );

    //The Associations below have been created with all possible keys, those that are not needed can be removed

/**
 * belongsTo associations
 *
 * @var array
 */
    public $belongsTo = array(
        'Solicitacao' => array(
            'className' => 'Solicitacao',
            'foreignKey' => 'solicitacao_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
        ),
        'Usuario' => array(
            'className' => 'Usuario',
            'foreignKey' => 'usuario_id',
            'conditions' => '',
            'fields' => '',
            'order' => '',
        ),
    );

    public function totalPontosPendentes($usuario_id)
    {

        $this->virtualFields = array(
            'total' => 'sum(SolicitacaoPonto.ponto)',
        );

        $arrRetorno = $this->find('first',
            array(
                'conditions' => array(
                    'SolicitacaoPonto.usuario_id' => $usuario_id,
                    'Solicitacao.status_id' => '1',
                ),
            )
        );
        return floor($arrRetorno['SolicitacaoPonto']['total']);
    }

    public function totalPontosConfirmados($usuario_id)
    {

        $this->virtualFields = array(
            'total' => 'sum(SolicitacaoPonto.ponto)',
        );

        $arrRetorno = $this->find('first',
            array(
                'conditions' => array(
                    'SolicitacaoPonto.usuario_id' => $usuario_id,
                    'Solicitacao.status_id' => '3',
                ),
            )
        );
        return floor($arrRetorno['SolicitacaoPonto']['total']);
    }
}
