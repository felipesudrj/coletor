<?php
App::uses('AppModel', 'Model');
/**
 * Estado Model
 *
 */
class Estado extends AppModel {

/**
 * Use table
 *
 * @var mixed False or table name
 */
	public $useTable = 'estado';

/**
 * Primary key field
 *
 * @var string
 */
	public $primaryKey = 'estado_id';

/**
 * Validation rules
 *
 * @var array
 */
	public $validate = array(
		'descricao' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
		'uf' => array(
			'notEmpty' => array(
				'rule' => array('notEmpty'),
				//'message' => 'Your custom message here',
				//'allowEmpty' => false,
				//'required' => false,
				//'last' => false, // Stop validation after this rule
				//'on' => 'create', // Limit validation to 'create' or 'update' operations
			),
		),
	);
}
