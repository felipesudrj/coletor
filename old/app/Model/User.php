<?php
App::uses('AppModel', 'Model');
App::uses('AuthComponent', 'Controller/Component');

/**
 * User Model
 *
 * @property Usuario $Usuario
 */
class User extends AppModel
{

    public $hasOne = array(
        'Usuario' => array(
            'className'  => 'Usuario',
            'foreignKey' => 'user_id',
        ),
    );

    public $validate = array(
        'username'         => array(
            'email' => array(
                'rule'    => array('email'),
                'message' => 'Informe um email valido',
                //'allowEmpty' => false,
                //'required' => false,
                //'last' => false, // Stop validation after this rule
                'on'      => 'create', // Limit validation to 'create' or 'update' operations
            ),
        ),
        'password'         => array(
            'notEmpty'       => array(
                'rule'    => array('notEmpty'),
                'message' => 'Não foi informado nenhum senha',
            ),
            'matchPasswords' => array(
                'rule'    => 'matchPasswords',
                'message' => 'As senhas digitadas não são iguais',
            ),
            'min_length'     => array(
                'rule'    => array('minLength', '6'),
                'message' => 'A senha deve conter 6 dígitos no mínimo!',
            ),
        ),
        'confirm_password' => array(
            'notEmpty' => array(
                'rule'    => array('notEmpty'),
                'message' => 'Confirme sua senha aqui',
            ),
        ),
    );

    public function matchPasswords($data)
    {
        if ($data['password'] == $this->data['User']['confirm_password']) {
            return true;
        }
        return false;
    }

    public function beforeFind($query = array())
    {
        if (isset($query['conditions']['User.password'])) {
            $query['conditions']['User.password'] = AuthComponent::password($query['conditions']['User.password']);
        };
        return $query;
    }

    public function beforeSave($options = array())
    {

        if (isset($this->data['User']['password'])) {
            $this->data['User']['password'] = AuthComponent::password($this->data['User']['password']);
        }

        return true;
    }

}
