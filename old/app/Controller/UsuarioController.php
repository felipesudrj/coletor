<?php
App::uses('AppController', 'Controller');
App::uses('UsersController', 'Controller');

/**
 * Users Controller
 *
 * @property User $User
 * @property PaginatorComponent $Paginator
 */
class UsuarioController extends AppController
{
    public $uses = array('User', 'Estado', 'Bairro', 'Estado', 'Cidade', 'Endereco', 'Usuario');

    public function perfil()
    {
        //Pegar ID do usuário logado
        $usuario_id = $this->Auth->user('Usuario.id');
        $user_id = $this->Auth->user('id');

        //Se for um post então realiza as alterações
        if ($this->request->isPost()) {
            $form = $this->request->data;

            $form['Usuario']['id'] = $usuario_id;
            $form['User']['id'] = $user_id;

            //Se o endereco_id for null então cadastrar o novo endereço
            if (empty($form['Usuario']['endereco_id'])) {
                $UsersController = new UsersController();
                $form['Usuario']['endereco_id'] = $UsersController->salvarEndereco($form);
            }

            $this->User->saveAll($form);

        }

        //Pegar dados de cadastro do usuário logado
        $usuario = $this->Usuario->getUsuario($usuario_id);

        $this->request->data = $usuario;

        //Pegar estados
        $estados = $this->Estado->find('list', array('fields' => array('estado_id', 'descricao')));

        //Setar variavéis
        $this->set(compact('estados'));
    }
}
