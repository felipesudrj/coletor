<?php
/**
 * BairroFixture
 *
 */
class BairroFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'bairro';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'bairro_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10, 'key' => 'primary'),
		'cidade_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10, 'key' => 'index'),
		'descricao' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 100, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'bairro_id', 'unique' => 1),
			'fk_cidade_bairro' => array('column' => 'cidade_id', 'unique' => 0)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'bairro_id' => 1,
			'cidade_id' => 1,
			'descricao' => 'Lorem ipsum dolor sit amet'
		),
	);

}
