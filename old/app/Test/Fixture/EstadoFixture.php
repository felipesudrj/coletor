<?php
/**
 * EstadoFixture
 *
 */
class EstadoFixture extends CakeTestFixture {

/**
 * Table name
 *
 * @var string
 */
	public $table = 'estado';

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'estado_id' => array('type' => 'integer', 'null' => false, 'default' => null, 'length' => 10, 'key' => 'primary'),
		'descricao' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 20, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'uf' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 2, 'collate' => 'latin1_swedish_ci', 'charset' => 'latin1'),
		'indexes' => array(
			'PRIMARY' => array('column' => 'estado_id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'latin1', 'collate' => 'latin1_swedish_ci', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'estado_id' => 1,
			'descricao' => 'Lorem ipsum dolor ',
			'uf' => ''
		),
	);

}
