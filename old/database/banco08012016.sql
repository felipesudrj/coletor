/*
SQLyog Ultimate v10.00 Beta1
MySQL - 5.5.43-log : Database - coletor
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `bairro` */

DROP TABLE IF EXISTS `bairro`;

CREATE TABLE `bairro` (
  `bairro_id` int(10) NOT NULL AUTO_INCREMENT,
  `cidade_id` int(10) NOT NULL,
  `descricao` varchar(100) NOT NULL,
  PRIMARY KEY (`bairro_id`),
  KEY `fk_cidade_bairro` (`cidade_id`),
  CONSTRAINT `fk_cidade_bairro` FOREIGN KEY (`cidade_id`) REFERENCES `cidade` (`cidade_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

/*Table structure for table `cidade` */

DROP TABLE IF EXISTS `cidade`;

CREATE TABLE `cidade` (
  `cidade_id` int(10) NOT NULL AUTO_INCREMENT,
  `estado_id` int(10) NOT NULL,
  `descricao` varchar(100) NOT NULL,
  `ddd` char(2) NOT NULL,
  PRIMARY KEY (`cidade_id`),
  KEY `fk_estado_cidade` (`estado_id`),
  CONSTRAINT `fk_estado_cidade` FOREIGN KEY (`estado_id`) REFERENCES `estado` (`estado_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;

/*Table structure for table `endereco` */

DROP TABLE IF EXISTS `endereco`;

CREATE TABLE `endereco` (
  `endereco_id` int(10) NOT NULL AUTO_INCREMENT,
  `bairro_id` int(10) NOT NULL,
  `cep` char(8) NOT NULL,
  `logradouro` varchar(200) NOT NULL,
  PRIMARY KEY (`endereco_id`),
  KEY `fk_bairro_endereco` (`bairro_id`),
  KEY `fk_cep` (`cep`),
  CONSTRAINT `fk_bairro_endereco` FOREIGN KEY (`bairro_id`) REFERENCES `bairro` (`bairro_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Table structure for table `estado` */

DROP TABLE IF EXISTS `estado`;

CREATE TABLE `estado` (
  `estado_id` int(10) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(20) NOT NULL,
  `uf` char(2) NOT NULL,
  PRIMARY KEY (`estado_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Table structure for table `grupo_material` */

DROP TABLE IF EXISTS `grupo_material`;

CREATE TABLE `grupo_material` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(255) DEFAULT NULL,
  `valor_kg` decimal(10,3) DEFAULT NULL,
  `valor_ponto` float DEFAULT NULL,
  `icone` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Table structure for table `material_tipo` */

DROP TABLE IF EXISTS `material_tipo`;

CREATE TABLE `material_tipo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `grupo_material_id` int(11) DEFAULT NULL,
  `descricao` varchar(255) DEFAULT NULL,
  `is_recicle` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_gru_mat` (`grupo_material_id`),
  CONSTRAINT `fk_gru_mat` FOREIGN KEY (`grupo_material_id`) REFERENCES `grupo_material` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=latin1;

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `role` varchar(20) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `ip` varchar(20) DEFAULT NULL,
  `aceita` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Table structure for table `usuario` */

DROP TABLE IF EXISTS `usuario`;

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `endereco_id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `telefone` varchar(255) DEFAULT NULL,
  `numero` varchar(255) NOT NULL,
  `complemento` text,
  `latitude` varchar(50) NOT NULL,
  `longitude` varchar(50) NOT NULL,
  `data_nascimento` date DEFAULT NULL,
  PRIMARY KEY (`id`,`nome`),
  KEY `fk_end_id` (`endereco_id`),
  KEY `fk_end_usu_id` (`user_id`),
  CONSTRAINT `fk_end_id` FOREIGN KEY (`endereco_id`) REFERENCES `endereco` (`endereco_id`),
  CONSTRAINT `fk_end_usu_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
