/*
SQLyog Ultimate v10.00 Beta1
MySQL - 5.5.43-log : Database - coletor
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `bairro` */

DROP TABLE IF EXISTS `bairro`;

CREATE TABLE `bairro` (
  `bairro_id` int(10) NOT NULL AUTO_INCREMENT,
  `cidade_id` int(10) NOT NULL,
  `descricao` varchar(100) NOT NULL,
  PRIMARY KEY (`bairro_id`),
  KEY `fk_cidade_bairro` (`cidade_id`),
  CONSTRAINT `fk_cidade_bairro` FOREIGN KEY (`cidade_id`) REFERENCES `cidade` (`cidade_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

/*Data for the table `bairro` */

LOCK TABLES `bairro` WRITE;

insert  into `bairro`(`bairro_id`,`cidade_id`,`descricao`) values (13,18,'Velha');

UNLOCK TABLES;

/*Table structure for table `cidade` */

DROP TABLE IF EXISTS `cidade`;

CREATE TABLE `cidade` (
  `cidade_id` int(10) NOT NULL AUTO_INCREMENT,
  `estado_id` int(10) NOT NULL,
  `descricao` varchar(100) NOT NULL,
  `ddd` char(2) NOT NULL,
  PRIMARY KEY (`cidade_id`),
  KEY `fk_estado_cidade` (`estado_id`),
  CONSTRAINT `fk_estado_cidade` FOREIGN KEY (`estado_id`) REFERENCES `estado` (`estado_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

/*Data for the table `cidade` */

LOCK TABLES `cidade` WRITE;

insert  into `cidade`(`cidade_id`,`estado_id`,`descricao`,`ddd`) values (18,1,'Blumenau','');

UNLOCK TABLES;

/*Table structure for table `endereco` */

DROP TABLE IF EXISTS `endereco`;

CREATE TABLE `endereco` (
  `endereco_id` int(10) NOT NULL AUTO_INCREMENT,
  `bairro_id` int(10) NOT NULL,
  `cep` char(8) NOT NULL,
  `logradouro` varchar(200) NOT NULL,
  PRIMARY KEY (`endereco_id`),
  KEY `fk_bairro_endereco` (`bairro_id`),
  KEY `fk_cep` (`cep`),
  CONSTRAINT `fk_bairro_endereco` FOREIGN KEY (`bairro_id`) REFERENCES `bairro` (`bairro_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `endereco` */

LOCK TABLES `endereco` WRITE;

insert  into `endereco`(`endereco_id`,`bairro_id`,`cep`,`logradouro`) values (6,13,'05289220','teste');

UNLOCK TABLES;

/*Table structure for table `estado` */

DROP TABLE IF EXISTS `estado`;

CREATE TABLE `estado` (
  `estado_id` int(10) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(20) NOT NULL,
  `uf` char(2) NOT NULL,
  PRIMARY KEY (`estado_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

/*Data for the table `estado` */

LOCK TABLES `estado` WRITE;

insert  into `estado`(`estado_id`,`descricao`,`uf`) values (1,'Santa Catarina','SC'),(2,'São Paulo','SP');

UNLOCK TABLES;

/*Table structure for table `grupo_material` */

DROP TABLE IF EXISTS `grupo_material`;

CREATE TABLE `grupo_material` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(255) DEFAULT NULL,
  `valor_kg` decimal(10,3) DEFAULT NULL,
  `valor_ponto` float DEFAULT NULL,
  `icone` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `grupo_material` */

LOCK TABLES `grupo_material` WRITE;

insert  into `grupo_material`(`id`,`descricao`,`valor_kg`,`valor_ponto`,`icone`) values (1,'Papel','0.030',10,NULL),(2,'Plastico','0.070',15,NULL),(3,'Vidro','0.030',20,NULL),(4,'Metais','0.030',25,NULL),(5,'Óleo','0.050',5.5,NULL),(6,'Lixo Eletronico','0.500',10,NULL);

UNLOCK TABLES;

/*Table structure for table `material_tipo` */

DROP TABLE IF EXISTS `material_tipo`;

CREATE TABLE `material_tipo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `grupo_material_id` int(11) DEFAULT NULL,
  `descricao` varchar(255) DEFAULT NULL,
  `is_recicle` tinyint(1) DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `fk_gru_mat` (`grupo_material_id`),
  CONSTRAINT `fk_gru_mat` FOREIGN KEY (`grupo_material_id`) REFERENCES `grupo_material` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=latin1;

/*Data for the table `material_tipo` */

LOCK TABLES `material_tipo` WRITE;

insert  into `material_tipo`(`id`,`grupo_material_id`,`descricao`,`is_recicle`) values (3,1,'Jornais e Revistas',1),(4,1,'Listas Telefônicas',1),(5,1,'Papel Sulfite/Rascunho',1),(6,1,'Papel de Fax',1),(7,1,'Folhas de Caderno',1),(8,1,'Formulários de Computador',1),(9,1,'Caixas em Geral (ondulado)',1),(10,1,'Aparas de Papel Guardanapos',1),(11,1,'Fotocópias',1),(12,1,'Envelopes',1),(13,1,'Rascunhos',1),(14,1,'Cartazes Velhos  ',1),(15,1,'Caixa de Pizza',1),(16,1,'Cartolinas e papel cartão',1),(17,1,'Papéis Sanitários (papel higiênico)',0),(18,1,'Papéis Plastificados',0),(19,1,'Papéis engordurados',0),(20,1,'Etiquetas adesivas',0),(21,1,'Papéis Parafinados',0),(22,1,'Papel carbono',0),(23,1,'Papel celofane',0),(24,1,'Bitucas de Cigarros',0),(25,1,'Fotografias',0),(26,4,'clipes',0),(27,4,'grampos',0),(28,4,'esponjas de aço',0),(29,4,'latas de tintas',0),(30,4,'latas de combustível e pilhas.',0),(31,2,'acrílicos.',0),(32,2,'teclados de computador',0),(33,2,'espuma',0),(34,2,'adesivos',0),(35,2,'isopor',0),(36,2,'tomadas',0),(37,2,'cabos de panela',0),(38,3,'espelhos',0),(39,3,'cristal',0),(40,3,'ampolas de medicamentos',0),(41,3,'cerâmicas e louças',0),(42,3,'lâmpadas',0),(43,3,'vidros temperados planos.',0),(44,2,'tampas',1),(45,2,'potes de alimentos (margarina)',1),(46,2,'frascos',1),(47,2,'utilidades domésticas',1),(48,2,'embalagens de refrigerante',1),(49,2,'garrafas de água mineral',1),(50,2,'recipientes para produtos de higiene e limpeza',1),(51,2,'PVC',1),(52,2,'tubos e conexões',1),(53,2,'sacos plásticos em geral',1),(54,2,'peças de brinquedos',1),(55,2,'engradados de bebidas',1),(56,2,'baldes',1),(57,2,'Embalagens Tetra Pak.',1),(58,4,'latas de alumínio (ex. latas de bebidas)',1),(59,4,'ferragens',1),(60,4,'canos',1),(61,4,'esquadrias',1),(62,4,'molduras de quadros',1),(63,3,'tampas',1),(64,3,'potes ',1),(65,3,'frascos',1),(66,3,'garrafas de bebidas',1),(67,3,'copos',1),(68,3,'embalagens',1);

UNLOCK TABLES;

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `role` varchar(20) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `ip` varchar(20) DEFAULT NULL,
  `aceita` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `users` */

LOCK TABLES `users` WRITE;

insert  into `users`(`id`,`username`,`password`,`role`,`created`,`modified`,`ip`,`aceita`) values (5,'teste@teste.com.br','ad7fe7e172a786a53644bce71ec1956019a38123',NULL,'2015-12-03 13:18:31','2015-12-03 13:18:31',NULL,NULL);

UNLOCK TABLES;

/*Table structure for table `usuario` */

DROP TABLE IF EXISTS `usuario`;

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `endereco_id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `telefone` varchar(255) DEFAULT NULL,
  `numero` varchar(255) NOT NULL,
  `complemento` text,
  `latitude` varchar(50) NOT NULL,
  `longitude` varchar(50) NOT NULL,
  `data_nascimento` date DEFAULT NULL,
  PRIMARY KEY (`id`,`nome`),
  KEY `fk_end_id` (`endereco_id`),
  KEY `fk_end_usu_id` (`user_id`),
  CONSTRAINT `fk_end_id` FOREIGN KEY (`endereco_id`) REFERENCES `endereco` (`endereco_id`),
  CONSTRAINT `fk_end_usu_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `usuario` */

LOCK TABLES `usuario` WRITE;

insert  into `usuario`(`id`,`user_id`,`endereco_id`,`nome`,`telefone`,`numero`,`complemento`,`latitude`,`longitude`,`data_nascimento`) values (5,5,6,'teste',NULL,'teste','teste','-26.9218546','-49.1199463',NULL);

UNLOCK TABLES;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
