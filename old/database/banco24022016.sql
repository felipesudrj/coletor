/*
SQLyog Ultimate v10.00 Beta1
MySQL - 5.5.43-log : Database - coletor
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
/*Table structure for table `agenda_disponivel` */

DROP TABLE IF EXISTS `agenda_disponivel`;

CREATE TABLE `agenda_disponivel` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `solicitacao_id` int(11) NOT NULL,
  `dia_semana` int(11) NOT NULL,
  `horario` int(11) NOT NULL,
  `create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `fk_solic` (`solicitacao_id`),
  CONSTRAINT `fk_solic` FOREIGN KEY (`solicitacao_id`) REFERENCES `solicitacao` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=latin1;

/*Data for the table `agenda_disponivel` */

LOCK TABLES `agenda_disponivel` WRITE;

insert  into `agenda_disponivel`(`id`,`solicitacao_id`,`dia_semana`,`horario`,`create`) values (37,20,0,0,'0000-00-00 00:00:00'),(38,20,0,1,'0000-00-00 00:00:00'),(39,20,0,2,'0000-00-00 00:00:00'),(40,20,2,0,'0000-00-00 00:00:00'),(41,20,2,1,'0000-00-00 00:00:00'),(42,20,2,2,'0000-00-00 00:00:00'),(43,20,4,0,'0000-00-00 00:00:00'),(44,20,4,1,'0000-00-00 00:00:00'),(45,20,4,2,'0000-00-00 00:00:00'),(46,21,0,0,'0000-00-00 00:00:00'),(47,21,0,1,'0000-00-00 00:00:00'),(48,21,0,2,'0000-00-00 00:00:00'),(49,21,2,0,'0000-00-00 00:00:00'),(50,21,2,1,'0000-00-00 00:00:00'),(51,21,2,2,'0000-00-00 00:00:00'),(52,21,4,0,'0000-00-00 00:00:00'),(53,21,4,1,'0000-00-00 00:00:00'),(54,21,4,2,'0000-00-00 00:00:00'),(55,22,0,0,'0000-00-00 00:00:00'),(56,22,0,1,'0000-00-00 00:00:00'),(57,22,0,2,'0000-00-00 00:00:00'),(58,22,2,0,'0000-00-00 00:00:00'),(59,22,2,1,'0000-00-00 00:00:00'),(60,22,2,2,'0000-00-00 00:00:00'),(61,22,4,0,'0000-00-00 00:00:00'),(62,22,4,1,'0000-00-00 00:00:00'),(63,22,4,2,'0000-00-00 00:00:00');

UNLOCK TABLES;

/*Table structure for table `bairro` */

DROP TABLE IF EXISTS `bairro`;

CREATE TABLE `bairro` (
  `bairro_id` int(10) NOT NULL AUTO_INCREMENT,
  `cidade_id` int(10) NOT NULL,
  `descricao` varchar(100) NOT NULL,
  PRIMARY KEY (`bairro_id`),
  KEY `fk_cidade_bairro` (`cidade_id`),
  CONSTRAINT `fk_cidade_bairro` FOREIGN KEY (`cidade_id`) REFERENCES `cidade` (`cidade_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

/*Data for the table `bairro` */

LOCK TABLES `bairro` WRITE;

insert  into `bairro`(`bairro_id`,`cidade_id`,`descricao`) values (15,20,'Velha Central');

UNLOCK TABLES;

/*Table structure for table `cidade` */

DROP TABLE IF EXISTS `cidade`;

CREATE TABLE `cidade` (
  `cidade_id` int(10) NOT NULL AUTO_INCREMENT,
  `estado_id` int(10) NOT NULL,
  `descricao` varchar(100) NOT NULL,
  `ddd` char(2) NOT NULL,
  PRIMARY KEY (`cidade_id`),
  KEY `fk_estado_cidade` (`estado_id`),
  CONSTRAINT `fk_estado_cidade` FOREIGN KEY (`estado_id`) REFERENCES `estado` (`estado_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;

/*Data for the table `cidade` */

LOCK TABLES `cidade` WRITE;

insert  into `cidade`(`cidade_id`,`estado_id`,`descricao`,`ddd`) values (20,3,'Blumenau','');

UNLOCK TABLES;

/*Table structure for table `endereco` */

DROP TABLE IF EXISTS `endereco`;

CREATE TABLE `endereco` (
  `endereco_id` int(10) NOT NULL AUTO_INCREMENT,
  `bairro_id` int(10) NOT NULL,
  `cep` char(8) NOT NULL,
  `logradouro` varchar(200) NOT NULL,
  PRIMARY KEY (`endereco_id`),
  KEY `fk_bairro_endereco` (`bairro_id`),
  KEY `fk_cep` (`cep`),
  CONSTRAINT `fk_bairro_endereco` FOREIGN KEY (`bairro_id`) REFERENCES `bairro` (`bairro_id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

/*Data for the table `endereco` */

LOCK TABLES `endereco` WRITE;

insert  into `endereco`(`endereco_id`,`bairro_id`,`cep`,`logradouro`) values (8,15,'89040160','Rua Bagé');

UNLOCK TABLES;

/*Table structure for table `estado` */

DROP TABLE IF EXISTS `estado`;

CREATE TABLE `estado` (
  `estado_id` int(10) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(20) NOT NULL,
  `uf` char(2) NOT NULL,
  PRIMARY KEY (`estado_id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

/*Data for the table `estado` */

LOCK TABLES `estado` WRITE;

insert  into `estado`(`estado_id`,`descricao`,`uf`) values (3,'Santa Catarina','SC');

UNLOCK TABLES;

/*Table structure for table `solicitacao` */

DROP TABLE IF EXISTS `solicitacao`;

CREATE TABLE `solicitacao` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tipo_material_id` int(11) NOT NULL,
  `unidade_medida_id` int(11) NOT NULL,
  `usuario_id` int(11) NOT NULL,
  `status_id` int(11) DEFAULT NULL COMMENT '1,2 ou 3',
  `quantidade` decimal(10,0) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tipo_mat` (`tipo_material_id`),
  KEY `fk_uni_med` (`unidade_medida_id`),
  KEY `fk_usua` (`usuario_id`),
  KEY `fk_statu` (`status_id`),
  CONSTRAINT `fk_tipo_mat` FOREIGN KEY (`tipo_material_id`) REFERENCES `tipo_material` (`id`),
  CONSTRAINT `fk_uni_med` FOREIGN KEY (`unidade_medida_id`) REFERENCES `unidade_medida` (`id`),
  CONSTRAINT `fk_usua` FOREIGN KEY (`usuario_id`) REFERENCES `usuario` (`id`) ON DELETE CASCADE,
  CONSTRAINT `fk_statu` FOREIGN KEY (`status_id`) REFERENCES `status` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

/*Data for the table `solicitacao` */

LOCK TABLES `solicitacao` WRITE;

insert  into `solicitacao`(`id`,`tipo_material_id`,`unidade_medida_id`,`usuario_id`,`status_id`,`quantidade`) values (20,1,1,6,1,'150'),(21,2,1,6,1,'150'),(22,3,2,6,1,'3');

UNLOCK TABLES;

/*Table structure for table `status` */

DROP TABLE IF EXISTS `status`;

CREATE TABLE `status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `status` */

LOCK TABLES `status` WRITE;

insert  into `status`(`id`,`descricao`) values (1,'Pendente de visualização por parte da cooperativa'),(2,'Visualizado pela cooperativa'),(3,'Confirmado coleta'),(4,'Pontuação pendente'),(5,'Pontuação confirmada');

UNLOCK TABLES;

/*Table structure for table `tipo_material` */

DROP TABLE IF EXISTS `tipo_material`;

CREATE TABLE `tipo_material` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(255) NOT NULL,
  `bgcolor` varchar(50) NOT NULL,
  `icone` varchar(50) NOT NULL,
  `imagem` varchar(50) DEFAULT NULL,
  `observacoes` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `tipo_material` */

LOCK TABLES `tipo_material` WRITE;

insert  into `tipo_material`(`id`,`descricao`,`bgcolor`,`icone`,`imagem`,`observacoes`) values (1,'Plastico','servive-block-green','icon-bell',NULL,NULL),(2,'Papel','servive-block-red','icon-bullhorn',NULL,NULL),(3,'Metal','servive-block-sea','icon-lightbulb',NULL,NULL),(4,'Vidro','servive-block-grey','icon-globe',NULL,NULL);

UNLOCK TABLES;

/*Table structure for table `unidade_medida` */

DROP TABLE IF EXISTS `unidade_medida`;

CREATE TABLE `unidade_medida` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `descricao` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `unidade_medida` */

LOCK TABLES `unidade_medida` WRITE;

insert  into `unidade_medida`(`id`,`descricao`) values (1,'Gramas'),(2,'Kilos'),(3,'Toneladas'),(4,'Litros'),(5,'Unidades');

UNLOCK TABLES;

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `role` varchar(20) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  `ip` varchar(20) DEFAULT NULL,
  `aceita` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `users` */

LOCK TABLES `users` WRITE;

insert  into `users`(`id`,`username`,`password`,`role`,`created`,`modified`,`ip`,`aceita`) values (6,'felipe.devel@gmail.com','ad7fe7e172a786a53644bce71ec1956019a38123',NULL,'2016-02-23 15:26:42','2016-02-23 21:13:18',NULL,NULL);

UNLOCK TABLES;

/*Table structure for table `usuario` */

DROP TABLE IF EXISTS `usuario`;

CREATE TABLE `usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `endereco_id` int(11) NOT NULL,
  `nome` varchar(255) NOT NULL,
  `telefone` varchar(255) DEFAULT NULL,
  `numero` varchar(255) NOT NULL,
  `complemento` text,
  `latitude` varchar(50) NOT NULL,
  `longitude` varchar(50) NOT NULL,
  `data_nascimento` date DEFAULT NULL,
  PRIMARY KEY (`id`,`nome`),
  KEY `fk_end_id` (`endereco_id`),
  KEY `fk_end_usu_id` (`user_id`),
  CONSTRAINT `fk_end_id` FOREIGN KEY (`endereco_id`) REFERENCES `endereco` (`endereco_id`),
  CONSTRAINT `fk_end_usu_id` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

/*Data for the table `usuario` */

LOCK TABLES `usuario` WRITE;

insert  into `usuario`(`id`,`user_id`,`endereco_id`,`nome`,`telefone`,`numero`,`complemento`,`latitude`,`longitude`,`data_nascimento`) values (6,6,8,'Felipe','(47)9746-6287','476','','-26.9233361','-49.1216197','0000-00-00');

UNLOCK TABLES;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
